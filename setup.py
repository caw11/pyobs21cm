'''
Created on 1 May 2020
@author: Catherine Allin Watkinson
Setup script
'''

#from setuptools import setup, find_packages
from distutils.core import setup

setup(name='pyobs21',
      version='1.0',
      author='Catherine Allin Watkinson',
      author_email='catherine.watkinson@gmail.com',
      package_dir = {'pyobs21' : 'src'},
      packages=['pyobs21'],
      package_data={'share':['*'],},
      install_requires=['numpy','scipy','pathlib','h5py','astropy'],
      #include_package_data=True,
)
