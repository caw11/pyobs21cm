import aipy as a, numpy as n, os
from numpy import sin,cos,pi

class AntennaArray(a.pol.AntennaArray):
    def __init__(self, *args, **kwargs):
        a.pol.AntennaArray.__init__(self, *args, **kwargs)
        self.array_params = {}
    def get_ant_params(self, ant_prms={'*':'*'}):
        prms = a.fit.AntennaArray.get_params(self, ant_prms)
        for k in ant_prms:
            top_pos = n.dot(self._eq2zen, self[int(k)].pos)
            if ant_prms[k] == '*':
                prms[k].update({'top_x':top_pos[0], 'top_y':top_pos[1], 'top_z':top_pos[2]})
            else:
                for val in ant_prms[k]:
                    if   val == 'top_x': prms[k]['top_x'] = top_pos[0]
                    elif val == 'top_y': prms[k]['top_y'] = top_pos[1]
                    elif val == 'top_z': prms[k]['top_z'] = top_pos[2]
        return prms
    def set_ant_params(self, prms):
        changed = a.fit.AntennaArray.set_params(self, prms)
        for i, ant in enumerate(self):
            ant_changed = False
            top_pos = n.dot(self._eq2zen, ant.pos)
            try:
                top_pos[0] = prms[str(i)]['top_x']
                ant_changed = True
            except(KeyError): pass
            try:
                top_pos[1] = prms[str(i)]['top_y']
                ant_changed = True
            except(KeyError): pass
            try:
                top_pos[2] = prms[str(i)]['top_z']
                ant_changed = True
            except(KeyError): pass
            if ant_changed: ant.pos = n.dot(n.linalg.inv(self._eq2zen), top_pos)
            changed |= ant_changed
        return changed
    def get_arr_params(self):
        return self.array_params
    def set_arr_params(self, prms):
        for param in prms:
            self.array_params[param] = prms[param]
            if param == 'dish_size_in_lambda':
                FWHM = 2.35*(0.45/prms[param]) #radians
                self.array_params['obs_duration'] = 60.*FWHM / (15.*a.const.deg)# minutes it takes the sky to drift through beam FWHM
            if param == 'antpos':
                bl_lens = n.sum(n.array(prms[param])**2,axis=1)**.5
        return self.array_params

#===========================ARRAY SPECIFIC PARAMETERS==========================

#Set antenna positions here; for regular arrays like Hera we can use an algorithm;
#otherwise antpos should just be a list of [x,y,z] coords in light-nanoseconds
def WGStoECEF(lat,long, h):
    ''' Take lat, long, height in WGS84 and
        return the corresponding ECEF X,Y,Z
        lat and long given in decimal degrees.
        altitude should be given in meters
        Source: http://wiki.gis.com/wiki/index.php/Geodetic_system'''

    lat = lat/180.0*pi; # converting to radians
    long = long/180.0*pi; # converting to radians
    a = 6378137.0; # earth semimajor axis in meters
    f = 1/298.257223563; # reciprocal flattening
    e2 = 2.0*f -f**2; # eccentricity squared

    chi = n.sqrt(1.0-e2*(sin(lat))**2.0);
    X = (a/chi +h)*cos(lat)*cos(long);
    Y = (a/chi +h)*cos(lat)*sin(long);
    Z = (a*(1-e2)/chi + h)*sin(lat);

    return X,Y,Z

def ECEFtoENU(refLat, refLong, refH, X, Y, Z):
    ''' convert ECEF coordinates to local east, north, up
        refLat and refLong given in decimal degrees.
        Source: http://wiki.gis.com/wiki/index.php/Geodetic_system
        Although note that I add the conversion to radians,
        which is clearly essential  CW '''

    # find reference location in ECEF coordinates
    Xr,Yr,Zr = WGStoECEF(refLat,refLong, refH);

    refLat = refLat/180.0*pi
    refLong = refLong/180.0*pi

    e = -sin(refLong)*(X-Xr) + cos(refLong)*(Y-Yr);
    n = -sin(refLat)*cos(refLong)*(X-Xr) - sin(refLat)*sin(refLong)*(Y-Yr) + cos(refLat)*(Z-Zr);
    u = cos(refLat)*cos(refLong)*(X-Xr) + cos(refLat)*sin(refLong)*(Y-Yr) + sin(refLat)*(Z-Zr);

    return e,n,u

m_to_ns = (100./a.const.len_ns)

#ska = n.loadtxt('SKA_layout_2019/layout_ecef_SKA_all.txt')
ska2 = n.loadtxt('SKA_layout_2019/layout_ecef_SKA_central.txt')
#ska3 = n.loadtxt('SKA_layout_2019/layout_ecef_SKA_core.txt')

ska = n.array(ska2)

xpos = n.array((ska[:,0]))
ypos = n.array((ska[:,1]))
zpos = n.array((ska[:,2]))

########### coord transform

stations_ = n.zeros((len(xpos),3))

#stations_
xpos, ypos, zpos = ECEFtoENU(xpos[0], ypos[0], zpos[0], xpos, ypos, zpos)

############# centering
x0 = xpos[0]
y0 = ypos[0]
z0 = zpos[0]
antpos = []
for i in range(len(xpos)):
    xpos[i] = (xpos[i] - x0)*m_to_ns
    ypos[i] = (ypos[i] - y0)*m_to_ns
    zpos[i] = (zpos[i] - z0)*m_to_ns
    antpos.append( (xpos[i], ypos[i], zpos[i]) )

#Set other array parameters here
prms = {
    'name': os.path.basename(__file__)[:-3], #remove .py from filename
    'loc': ('-26:49:28.999','116:45:52.013'), # Miluera, WA converted with https://www.gps-coordinates.net/gps-coordinates-converter
    'antpos': antpos, #List of antenna position is ns
    'beam': a.fit.Beam2DGaussian,
    'dish_size_in_lambda': (38./2.0), #in units of wavelengths at 150 MHz = 2 meters; this will also define the observation duration
    'Trx': 40e3, #receiver temp in mK
}

#=======================END ARRAY SPECIFIC PARAMETERS==========================

def get_aa(freqs):
    '''Return the AntennaArray to be used for simulation.'''
    location = prms['loc']
    antennas = []
    nants = len(prms['antpos'])
    for i in range(nants):
        beam = prms['beam'](freqs, xwidth=(0.45/prms['dish_size_in_lambda']), ywidth=(0.45/prms['dish_size_in_lambda'])) #as it stands, the size of the beam as defined here is not actually used anywhere in this package, but is a necessary parameter for the aipy Beam2DGaussian object
        antennas.append(a.fit.Antenna(0, 0, 0, beam))
    aa = AntennaArray(prms['loc'], antennas)
    p = {}
    for i in range(nants):
        top_pos = prms['antpos'][i]
        p[str(i)] = {'top_x':top_pos[0], 'top_y':top_pos[1], 'top_z':top_pos[2]}
    aa.set_ant_params(p)
    aa.set_arr_params(prms)
    return aa

def get_catalog(*args, **kwargs): return a.src.get_catalog(*args, **kwargs)
