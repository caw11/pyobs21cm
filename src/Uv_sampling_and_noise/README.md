Written by Catherine Watkinson (catherine.watkinson <at> gmail <dot> com).
If you make use of this, please reference this repository and 21cmSENSE literature.

IMPORTANT: uv\_to\_k\_plus\_calc\_sense.py and 21cmSENSE require Python 2.7 unlike the rest of the package that requires Python 3.0

This includes a few uv-sampling-noise .h5 files (e.g. ska_central_2019.track_1.0hr_mod_z13.204_nu0.100.h5)
for use out of the box with the main PyObs21cm code.

It also includes uv_to_k_plus_calc_sense.py which can generate new uv-sampling-noise
files for different bandwidths or assumptions about the foregrounds.
The print_cmSENSE_flags() function in the main code can be used to get the
necessary flags for this.

I have also included in /21cmSense_mkarray_files/ the calibration file and
station positions needed to make new array files (such as ska_central_2019.track_1.0hr_blmin0_blmax1610_0.157GHz_arrayfile.npz)
using 21cmSENSE.

USAGE INSTRUCTIONS:
1. work out what redshifts you want to look at and use Python Interferometer\_calcs/OBS\_common\_conversions
to work out closest freq (and the redshift they correspond to since 21cmSENSE arrayfiles works with freq)
2. If you don't already have the appropriate arrayfiles (*\_arrayfile.npz), then make them using 21cmSENSE with
mk\_array\_file.py
    mk\_array\_file.py -C ska\_central\_2019 --track 1.0 --freq [freq in GHz]

2. Run e.g. bispec\_example\_two\_param\_coeval.py or any one of the PyObs21cm wrapper's
with your desired HII_DIM, BOX_LEN, CosmoParams() and with CMSENSE\_FLAGS\_ONLY = True
to get the appropriate nchan, bwidth, hlittle, omega\_m flags for your coeval cube.

3. Generate the uv\_noise file for each redshift (using the relevant flags and arrayfile) using:
    python uv\_to\_k\_plus\_calc\_sense.py --model mod --nchan 50  --bwidth 0.007086227595194771  --hlittle 0.6774  --omega\_m 0.3075
    ska512central.track\_1.0hr\_blmin0\_blmax1610\_0.157GHz\_arrayfile.npz
the --model flag is optional, without it the code assume 'opt', i.e. FG have been avoided.

These noisefiles can now be used by the main PyObs21 packages to make noisefiles, datafiles
or simply to add noise and uv sampling to your coeval cube (to use pass
  its location as Filepath argument).
