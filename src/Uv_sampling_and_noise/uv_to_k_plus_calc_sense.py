#! /usr/bin/env python
'''
uv_to_k_plus_calc_sense.py

REQUIREMENTS
python 2.7
scipy
aipy

Calculates the expected sensitivity of a 21cm experiment to a given 21cm power spectrum.
Requires as input an array .npz file created with 21cmSENSE's mk_array_file.py.

Adapted from 21cmSENSE calc_sense.py by C. Watkinson to output the full uv sampling
in comoving Mpc^{-1} along with the corresponding noise for a given baseline.
Please reference

You can find the necessary settings to run with simObs_21cmpy using
(See README for instructions on the Params files):
    import simObs_21cmpy as p21obs
    p21obs.print_cmSENSE_flags(UParams=UParams, CParams=CParams, z=z_mid)

Which will provide essential flags, e.g. --nchan 50  --bwidth 0.007086227595194771  --hlittle 0.6774  --omega_m 0.3075
Then to run, e.g.
    python uv_to_k_plus_calc_sense.py --model mod --nchan 50  --bwidth 0.007086227595194771  --hlittle 0.6774  --omega_m 0.3075 ska512central.track_1.0hr_blmin0_blmax1610_0.157GHz_arrayfile.npz

note if you run in --model 'opt' mode you might want to remove the buffer on the horizon
by adding the --buff 0.0 flag.
Three foreground options are 'pess' (all k modes inside horizon + buffer are excluded,
and all baselines are added incoherently),
'mod' (all k modes inside horizon + buffer are excluded,
but all baselines within a uv pixel are added coherently),
and 'opt' (all modes k modes inside the primary field of view are excluded)

Included /21cmSense_mkarray_files/ are SKA array files for use with 21cmSENSE's mk_array_file.py

Adapted from 21cmSENSE as committed on Jan 13, 2017 (ca649ba).
** NOTE THE OUTPUT HERE HAS BEEN ADAPTED FROM THE USUAL 21cmSENSE TO BE IN cMpc^{-1} **
'''
import os, sys
lib_path = os.path.abspath('../')
sys.path.append(lib_path)

import aipy as a, numpy as n, optparse, sys
from scipy import interpolate
import h5py
import convs_dists as conv

#import capo as C # This is a Paper package, using to check conversions

o = optparse.OptionParser()
o.set_usage('uv_to_k_plus_calc_sense.py [options] *.npz')
o.set_description(__doc__)
o.add_option('-m', '--model', dest='model', default='mod',
    help="The model of the foreground wedge to use.  Three options are 'pess' (all k modes inside horizon + buffer are excluded, and all baselines are added incoherently), 'mod' (all k modes inside horizon + buffer are excluded, but all baselines within a uv pixel are added coherently), and 'opt' (all modes k modes inside the primary field of view are excluded).  See Pober et al. 2014 for more details.")
o.add_option('-b', '--buff', dest='buff', default=0.1, type=float,
    help="The size of the additive buffer outside the horizon to exclude in the pessimistic and moderate models.")
o.add_option('--ndays', dest='ndays', default=180., type=float,
    help="The total number of days observed.  The default is 180, which is the maximum a particular R.A. can be observed in one year if one only observes at night.  The total observing time is ndays*n_per_day.")
o.add_option('--n_per_day', dest='n_per_day', default=6., type=float,
    help="The number of good observing hours per day.  This corresponds to the size of a low-foreground region in right ascension for a drift scanning instrument.  The total observing time is ndays*n_per_day.  Default is 6.  If simulating a tracked scan, n_per_day should be a multiple of the length of the track (i.e. for two three-hour tracks per day, n_per_day should be 6).")
o.add_option('--bwidth', dest='bwidth', default=0.008, type=float,
    help="Cosmological bandwidth in GHz.  Note this is not the total instrument bandwidth, but the redshift range that can be considered co-eval.  Default is 0.008 (8 MHz).")
o.add_option('--nchan', dest='nchan', default=64, type=int,
    help="Integer number of channels across cosmological bandwidth.  Defaults to 82, which is equivalent to 1024 channels over 100 MHz of bandwidth.  Sets maximum k_parallel that can be probed, but little to no overall effect on sensitivity.")
o.add_option('--boxlen', dest='boxlen', default=128, type=int,
    help="Box length in cMpc for the simulation you want noise-uv files for.")
o.add_option('--no_ns', dest='no_ns', action='store_true',
    help="Remove pure north/south baselines (u=0) from the sensitivity calculation.  These baselines can potentially have higher systematics, so excluding them represents a conservative choice.")
o.add_option('--hlittle', dest='hlittle', default=0.6774, type=float,
    help="The hubble parameter h where H0=100h, make sure this matches your simObs_21cmpy settings. Default is 0.6774.") #cw addition
o.add_option('--omega_m', dest='omega_m', default=0.3075, type=float,
    help="Omega_matter, make sure this matches your simObs_21cmpy settings. Default is 0.3075.") #cw addition

opts, args = o.parse_args(sys.argv[1:])

#=========================COSMOLOGY/BINNING FUNCTIONS=========================

def u_arcmins2deg(u_arcmins): # cw CHECK NOT SURE NECESSARY + since lightcones generally in degrees
    # converts passed u (in arcmins^{-1}) to degrees^{-1}
    arcmins2deg=0.0166667
    return (u_arcmins/arcmins2deg)

#Convert frequency (GHz) to redshift for 21cm line.
def f2z(fq):
    F21 = 1.42040575177
    return (F21 / fq - 1)

#Multiply by this to convert an angle (radians) on the sky to a transverse distance in Mpc/h at redshift z
def dL_dth(z):
    '''[h^-1 Mpc]/radian, from Furlanetto et al. (2006)''' #cw surely this should be /arcmins?
    return 1.9 * (1./ (a.const.arcmin) ) * ((1+z) / 10.)**.2

#Multiply by this to convert a bandwidth in GHz to a line of sight distance in Mpc/h at redshift z
def dL_df(z, omega_m=0.3075):
    '''[h^-1 Mpc]/GHz, from Furlanetto et al. (2006)'''
    return (1.7 / 0.1) * ((1+z) / 10.)**.5 * (omega_m/0.15)**-0.5 * 1e3

#Multiply by this to convert a baseline length in wavelengths (at the frequency corresponding to redshift z) into a tranverse k mode in h/Mpc at redshift z
def dk_du(z):
    '''2pi * [h Mpc^-1] / [wavelengths], valid for u >> 1.'''
    return 2*n.pi / dL_dth(z) # from du = 1/dth, which derives from du = d(sin(th)) using the small-angle approx

#Multiply by this to convert eta (FT of freq.; in 1/GHz) to line of sight k mode in h/Mpc at redshift z
def dk_deta(z, omega_m=0.3075):
    '''2pi * [h Mpc^-1] / [GHz^-1]'''
    return 2*n.pi / dL_df(z, omega_m)

#scalar conversion between observing and cosmological coordinates
def X2Y(z):
    '''[h^-3 Mpc^3] / [str * GHz]'''
    return dL_dth(z)**2 * dL_df(z)

#A function used for binning
def find_nearest(array,value):
    idx = (n.abs(array-value)).argmin()
    return idx

#====================OBSERVATION/COSMOLOGY PARAMETER VALUES====================

#Load in data from array file; see mk_array_file.py for definitions of the parameters
array = n.load(args[0])
name = array['name']
obs_duration = array['obs_duration']
dish_size_in_lambda = array['dish_size_in_lambda']
Trx = array['Trx']
t_int = array['t_int']
if opts.model == 'pess':
    uv_coverage = array['uv_coverage_pess']
else:
    uv_coverage = array['uv_coverage']

#h = 0.6774 #cw removed as now a passed as an opt
h = opts.hlittle
omega_m = opts.omega_m
B = opts.bwidth
z = f2z(array['freq'])
print "z = ", z

dish_size_in_lambda = dish_size_in_lambda*(array['freq']/.150) # linear frequency evolution, relative to 150 MHz
first_null = 1.22/dish_size_in_lambda #for an airy disk, even though beam model is Gaussian
bm = 1.13*(2.35*(0.45/dish_size_in_lambda))**2
nchan = opts.nchan
boxlen = opts.boxlen
#kpls = dk_deta(z, omega_m) * n.fft.fftfreq(nchan, B/nchan) #cw replace with klos

print 'Assuming a central redshift of z = ', z, ', and a number of channels = ', nchan

eta_los = n.fft.fftfreq(nchan, (B*1000.0)/nchan) # cw+ (note we have converted into MHz as that's what lightcones are in)
Tsky = 60e3 * (3e8/(array['freq']*1e9))**2.55  # sky temperature in mK
# Not the above may be overridden later in the code for the expression from Mellema2013
n_lstbins = opts.n_per_day*60./obs_duration # Number of independent samples of same piece of sky.

#===============================EOR MODEL===================================

#You can change this to have any model you want, as long as mk, mpk and p21 are returned

#This is a dimensionless power spectrum, i.e., Delta^2
#cw Note that the passed power spectrum, Delta^2, should be in mK^2
''' #cw removed dependence on power spectrum
modelfile = opts.eor
model = n.loadtxt(modelfile)
mk, mpk = model[:,0]/h, model[:,1] #k [h Mpc^{-1}], Delta^2(k)

#cw note that we will ultimately convert from Mpc/h to Mpc before read-out (unlike the original which works in and outputs in Mpc/h)

#interpolation function for the EoR model
p21 = interpolate.interp1d(mk, mpk, kind='linear')'''

#=================================MAIN CODE===================================

#sense will include sample variance, Tsense and Pnoise_per_base will be Thermal only
kx, ky, klos = [],[],[]
u_deg, v_deg, eta = [],[],[]

samps_kpl, Tsense = {}, {}
Pnoise_per_base, Tsys2 = [], []

uv_coverage *= t_int
SIZE = uv_coverage.shape[0]
print("uv_coverage shape = ", uv_coverage.shape)

# Cut unnecessary data out of uv coverage: auto-correlations & half of uv plane (which is not statistically independent for real sky)
''' #cw removed as I am working full fft's in Run_21cmBoxPS module
uv_coverage[SIZE/2,SIZE/2] = 0.
uv_coverage[:,:SIZE/2] = 0.
uv_coverage[SIZE/2:,SIZE/2] = 0.
'''
if opts.no_ns: uv_coverage[:,SIZE/2] = 0.

# cw calculate comoving distance to z for use in converting u to k (cMpc)
#cw make this an array and precalculate Dm for each freq bin to incl chromaticity accross box
D_comov = conv.Dm_cc(z=z, omega_m=omega_m, hlittle=h) # In fixing this for whole bandwidth I am ignoring the uv -> k evolution over the bandwidth in hand
kpls=n.zeros( len(eta_los) )
for i in range( len(eta_los) ):
    kpls[i] = ( conv.klos_from_eta( eta_cMHz=eta_los[i], z_mid=z, omega_m=omega_m, hlittle=h ) ) #eta_cMHz, z_mid, cosmo=None, omega_m=None, hlittle=None

print("kpls = ", kpls, "omega_m = ", omega_m, "hlittle = ", h)
#loop over uv_coverage to calculate k_pr
nonzero = n.where(uv_coverage > 0)

uv_sample_ct = 0
for iu,iv in zip(nonzero[1], nonzero[0]):

   #print("iu = ", iu, ", and uv_coverage [ [iu][iv] ] = ", uv_coverage[iv][iu] )
   u, v = (iu - SIZE/2) * dish_size_in_lambda, (iv - SIZE/2) * dish_size_in_lambda # cw this adjusts for the indexing in pyfftw relative to aipy

   umag = n.sqrt(u**2 + v**2)
   kpr = conv.kperp_from_uv(u_or_v=umag, z=z, omega_m=omega_m, hlittle=h, Dm=D_comov) #cw replacing umag * dk_du(z)

   #calculate horizon limit for baseline of length umag
   if opts.model in ['mod','pess']: hor = dk_deta(z, omega_m) * h * umag/array['freq'] + opts.buff #cw added h as our k are in 1/Mpc not h/Mpc
   elif opts.model in ['opt']: hor = dk_deta(z, omega_m) * h * (umag/array['freq'])*n.sin(first_null/2)
   else: print '%s is not a valid foreground model; Aborting...' % opts.model; sys.exit()
   if not Tsense.has_key(kpr): #cw onl initialise the kpl arrays if not already done so
       samps_kpl[kpr] = n.zeros_like(kpls)
       Tsense[kpr] = n.zeros_like(kpls)

   for i, kpl in enumerate(kpls):
       #exclude k_parallel modes contaminated by foregrounds
       k = n.sqrt(kpl**2 + kpr**2)
       #'''
       if n.abs(kpl) < hor:
           #print "Mode is in the wedge kpl = ", kpl, "hor = ", hor
           continue

       tot_integration = uv_coverage[iv,iu] * opts.ndays

       Tsys = Tsky + Trx
       Tsys = 100.0e3 + 300.0e3*(0.150/(array['freq']))**2.55 #cw uncheck For comparison with Mellema + 2013 http://arxiv.org/abs/1210.0197
       bm2 = bm/2. # beam^2 term calculated for Gaussian; see Parsons et al. 2014
       bm_eff = bm**2 / bm2 # this can obviously be reduced; it isn't for clarity
       # scalar = X2Y(z) * bm_eff * B * (k/h)**3 / (2.0*n.pi**2) - replaced since we want the noise power not delta^2
       #DEVEL_LIGHTCONE  - next line specific to coeval operation
       #B = 10.0/1000.0 # For the noise calculation, I want B=10MHz to return realistic noise rather than this being defined by my coeval size #cw updated Mar 2020
       scalar = X2Y(z) * bm_eff * B/nchan / (h**3) # h**3 converts X2Y into Mpc**3 from Mpc/h**3
       Trms = Tsys / n.sqrt( 2.0*(B/nchan*1e9)*tot_integration ) #cw updated Mar 2020 to use pixel as bandwidth
       #B = opts.bwidth # Need to revert this variable to be the passed value for the filename #cw updated Mar 2020

       #add errors in inverse quadrature
       samps_kpl[kpr][i] += 1. # keep count of how many samples of the k parallels the instrument has made
       Tsense[kpr][i] += 1./(scalar*Trms**2)**2

       #cw convertion of u and v to to kx and ky in co-moving [Mpc^-1]
       kx.append( conv.kperp_from_uv(u_or_v=u, z=z, omega_m=omega_m, hlittle=h, Dm=D_comov) ) #cw replaces (u*dk_du(z)*h) #cw convert u to kx Mpc^-1 (dk_du is in h Mpc^{-1} and u and v in arcmins)
       ky.append( conv.kperp_from_uv(u_or_v=v, z=z, omega_m=omega_m, hlittle=h, Dm=D_comov) ) #cw replaces (v*dk_du(z)*h) #cw convert v to ky Mpc^-1 (conversion tested against mine and both consistent)
       klos.append( kpl ) #cw replaces kpl*h) #cw convert klos into Mpc^-1
       eta.append( eta_los[i] )

       #Pnoise_per_base.append( (scalar*Trms**2*(2.0*n.pi**2))/k**3 /n_lstbins ) #cw Note we convert from Delta_N^2 to P_N^2 and divide by the number of independent los
       Pnoise_per_base.append( (scalar*Trms**2) ) #/ (n_lstbins + 0.0)  )   #cw updated Mar 2020 Note we convert from Delta_N^2 to P_N^2 and divide by the number of independent los
       # I originally had (scalar*Trms**2) / n_lstbins above but I don't think this is consistent with my noise sampling method, and certainly doesn't appear in the Mellema+2013 or Koopmans+2015 which use McQuinn's calculations
       Tsys2.append( Trms**2 )#cw +
       #if (kx[uv_sample_ct]<-1.0):
       #   print kx[uv_sample_ct], ky[uv_sample_ct], klos[uv_sample_ct]
       uv_sample_ct+=1
print "uv_sample_ct = ", uv_sample_ct

# Now we need to generate the number of samples in each kmag so that we can use it to estimate the sample variance error
#bin the result in 1D
delta = dk_deta(z)*(1./B) #default bin size is given by bandwidth
kmag = n.arange(delta,n.max(kpr),delta)

n_samps = n.zeros_like(kmag)
for ind, kpr in enumerate(Tsense.keys()):
    for i, kpl in enumerate(kpls):
        k = n.sqrt(kpl**2 + kpr**2)
        #add sample count for every uv-sample that falls in the given k-bin
        n_samps[find_nearest(kmag,k)] += samps_kpl[kpr][i] # should this be += (1+n_lstbins) since every u, v sample will sample three independent

for i, samp in enumerate(n_samps):
    n_samps[i] *= n_lstbins # Each baseline has sampled n_lstbins different regions of the sky so we must multiply to get total number of independent samples

# output file for the unbinned noise
with h5py.File('%s_%s_z%.3f_nu%.3f_nchan%d_bwidth%.4f_%dcMpc.h5' % (name, opts.model, z, array['freq'], nchan, B, boxlen), 'w') as hf:
    hf.create_dataset( 'kx_cMpc', data=n.array(kx) )
    hf.create_dataset( 'ky_cMpc', data=n.array(ky) )
    hf.create_dataset( 'klos_cMpc', data=n.array(klos) )
    hf.create_dataset( 'n_lst', data=n_lstbins )
    hf.create_dataset( 'eta_Mhz', data=n.array(eta) )
    hf.create_dataset( 'Pnoise', data=n.array(Pnoise_per_base) )
    hf.create_dataset( 'Tsys2_K2', data=n.array(Tsys2) )

with h5py.File('%s_%s_%s_z%.3f_nu%.3f_nchan%d_bwidth%.4f_%dcMpc.h5' % ("num_samps_per_kmag", name, opts.model, z, array['freq'], nchan, B, boxlen), 'w') as hf:
    hf.create_dataset( 'kmag', data=n.array(kmag) )
    hf.create_dataset( 'n_samps', data=n.array(n_samps) )
