'''
    convert_lightcones.py

    Contains various functions for converting from simulation lightcones to
    observational lightcones and vice versa.


    slice_cMpc_2_deg() -> converts a slice of cMpc at z and converts to degrees,
                           trims and resamples to requested FoV.
    slice_deg_2_cMpc() -> converts a slice of degrees at z and converts to cMpc,
                           trims and resamples to requested FoV.
    lc_cMpc_2_deg() -> converts a lightcone (or coeval) to one with constant deg and
                       if asked oututs them as GPR ready slices.

'''

DEBUG=0

from astropy.cosmology import FlatLambdaCDM, z_at_value
import astropy.units as au
import numpy as np
import scipy

from pyobs21 import convs_dists as cvdist
from pyobs21 import get_slice_zfreq as get_slice


NU_21CM = 1420.405751 #MHz

def make_header(data=None, z=None, L_los=None, L_perp=None, los_axis=2, mK=True, cosmo=None):
    '''
        makes header for use with lc_cMpc_2_deg()

        args:
        data (float n*n*n float) - datacube
        z_ctr (3 x 1 float) - [lowz, ctrz, hiz] redshift of either lowest z, central,
                              or highest z point of coeval (rest should be None,
                              e.g. if you want to pass central z, then pass z=[None, zctr, None])
        L_los (float) - cMpc for the line-of-sight side of the data
        L_perp (float) - cMpc for the on-the-sky side of the data
        los_axis (int) (opt) - axis which corresponds to line-of-sight
        mK (bool) (opt) - if mK (True) or K (False)
        cosmo (class) (opt) - instance of astropy FlatLambdaCDM (or similar) cosmology class

        returns:
        header_dict (dictionary) containing -
            ["Nside"] number pixels on side (for the xy slices)
            ["Lside"] Simulation side length in cMpc
            ["Dc_step"] step size in comoving Mpc
            ["Dc_min"] comoving distance of highest freq/lowest z slice
            ["BUNIT"] unit of data mK or K
    '''

    if (cosmo is None):
        cosmo = cvdist.cosmo_default()

    if ( (data is None) or (L_los is None) or (L_perp is None)):
        raise ValueError("convert_lightcones (make_header). USAGE: make_header(data, z, L_los)")

    header_dict={}
    Nside_z = data.shape[los_axis]
    if (los_axis==2):
        Nside_xy = data.shape[0]
    elif (los_axis==0):
        Nside_xy = data.shape[1]
    else:
        raise ValueError("convert_lightcones (make_header). USAGE: make_header(data, z, L_los)")

    if not( z[0] is None ):
        # User has passed low z so we can calculate the lowest comoving dist
        cdist_low = cosmo.comoving_distance(z[0]).value
    elif not( z[1] is None ):
        # User has passed the middle redshift, so we need to work out lowest comoving dist from that
        cdist_ctr = cosmo.comoving_distance(z[1]).value
        cdist_low = cdist_ctr - L_los/2.0
    elif not( z[2] is None ):
        # User has passed the highest redshift of datacube, so we need to work out lowest comoving dist from that
        cdist_ctr = cosmo.comoving_distance(z[1]).value
        cdist_low = cdist_ctr - L_los
    else:
        raise ValueError("convert_lightcones -> make_header. USAGE: make_header(data=None, z=None, L_los=None, cosmo=None). z=[lowz, ctrz, hiz] redshift of either lowest z, central, or highest z point of coeval (rest should be None, e.g. if you want to pass central z, then pass z=[None, zctr, None])")

    header_dict["Nside"] = Nside_xy #number pixels on side (for the xy slices)
    header_dict["Dc_step"] = ( L_los / ( Nside_z + 0.0 ) )#step size in comoving Mpc
    header_dict["Dc_min"] = cdist_low #comoving distance of highest freq/lowest z slice
    header_dict["Lside"] = L_perp

    if (mK is True):
        header_dict["BUNIT"]="mK" #unit of data mK or K
    else:
        header_dict["BUNIT"]="K" #unit of data mK or K

    return header_dict

def slice_cMpc_2_deg(data_slice=None, Lin_cMpc=None, z=None, cut_ctrl=False, Lout_deg=10.0, dim_out=256, cosmo=None):
    '''
    Converts a slice of cMpc at z and converts to degrees,
    trims and resamples to requested FoV in degrees.

    args:
    data_slice (n*n float) - data slice you which to convert
    Lin_cMpc (float) - FoV in cMpc of data_slice
    z (float) - redshift of slice
    cut_ctrl (bool) (opt default=False) - returns the bottom left slice by default, set to False if you want to return the central slice     Lout_deg (float) (opt default=10.0) - desired FoV for returned slice in degrees
    dim_out (float) (opt default=256) - desired resolution per side for returned slice
    cosmo (class) (opt) - instance of astropy FlatLambdaCDM (or similar) cosmology class

    Returns:
        data_slice_deg (dim_out*dim_out float) - slice with Lout_deg degrees and dim_out pixels per side
    '''

    if (cosmo is None):
        cosmo = cvdist.cosmo_default()

    cdist = cosmo.comoving_distance(z).value
    #print("slice_cMpc_2_deg -> z = ", z, ", cdist = ", cdist)
    fov_in_deg = (Lin_cMpc / cdist) * au.rad.to(au.deg)
    Nup = int( np.ceil(Lout_deg / fov_in_deg) )

    data_tiled = np.tile(data_slice, reps=(Nup, Nup))
    if (DEBUG>0):
        print( "slice_cMpc_2_deg -> Required dimension of ouput slice = : %d" % (dim_out) )
        print( "slice_cMpc_2_deg -> Input FoV: %s [deg], Tiling repeats: %d" % (fov_in_deg, Nup) )

    Nside1 = data_tiled.shape[0]
    Nside2 = Nside1 * Lout_deg / (fov_in_deg*Nup)
    if (DEBUG>0):
        print( "slice_cMpc_2_deg -> Nside1 & Nside2 = ", Nside1, Nside2, (Nside1 - Nside2)/2.0 )

    zoom = (dim_out+0.0) / Nside2  # +0.1 to workaround the scipy warning
    indx_cut = int( np.floor( (Nside1 - Nside2)/2.0 ) )
    if (cut_ctrl is True): # Return central slice of FoV
        if (DEBUG>0):
            print( "slice_cMpc_2_deg -> upsampling with zoom = ", zoom )
            print( "slice_cMpc_2_deg -> before trimming to centre, data shape = ", data_tiled.shape )
        if (indx_cut>0):
            img2_data = data_tiled[indx_cut:-indx_cut, indx_cut:-indx_cut] # cut out the central region with Nside2^2 pixels
        elif (indx_cut<0):
            print("slice_cMpc_2_deg -> Alert, there is something going on with the indexing for data field")
        else:
            img2_data = data_tiled
    else: # Return the bottom left slice
        if (indx_cut>0):
            indx2 = np.int( np.ceil(Nside2) )
            img2_data = data_tiled[0:indx2, 0:indx2] # cut out the central region with Nside2^2 pixels
        elif (indx_cut<0):
            raise ValueError("slice_cMpc_2_deg -> Alert, there is something going on with the indexing for data field")
        else:
            img2_data = data_tiled

    if (DEBUG>0):
        print( "slice_cMpc_2_deg -> After cutting out the central region) = ", img2_data.shape, " Use indx_cut = ", indx_cut)
    # Rescale to the output size
    data_slice = scipy.ndimage.zoom(img2_data, zoom=zoom, order=0, mode='wrap')
    if (DEBUG>0):
        print( "slice_cMpc_2_deg -> after zoom = ", data_slice.shape )
    if not( data_slice.shape[0] == dim_out ):
        idx_ceil = int( np.ceil( (data_slice.shape[0] - dim_out)/2.0 ) )
        idx_floor = int( np.floor( (data_slice.shape[0] - dim_out)/2.0 ) )
        if (DEBUG>0):
            print("slice_cMpc_2_deg -> index ceil & floor = ", idx_ceil, idx_floor)
        data_slice = data_slice[idx_floor:-idx_ceil, idx_floor:-idx_ceil]
    if (DEBUG>0):
        print( "slice_cMpc_2_deg -> after refining pixel trim = ", data_slice.shape )

    return data_slice, img2_data.shape[0]

def slice_deg_2_cMpc(data_slice=None, Lin_deg=None, z=None, cut_ctrl=False, Lout_cMpc=(1000.0/0.6774), dim_out=256, cosmo=None):
    '''
    data_slice (n*n float) - data slice you which to convert
    Lin_deg (float) - FoV in degrees of data_slice
    z (float) - redshift of
    cut_ctrl (bool) (opt default=False) - returns the bottom left slice by default, set to False if you want to return the central slice
    Lout_cMpc (float) (opt default=10.0) - desired FoV for returned slice in cMpc
    dim_out (float) (opt default=256) - desired resolution per side for returned slice
    cosmo (float) (opt) - instance of astropy FlatLambdaCDM (or similar) cosmology class

    Returns:
        data_slice_cMpc (dim_out*dim_out float) - slice with Lout_cMpc cMpc and dim_out pixels per side
    '''

    if (cosmo is None):
        cosmo = cvdist.cosmo_default()

    cdist = cosmo.comoving_distance(z).value
    fov_in_cMpc = np.radians(Lin_deg) * cdist
    if (DEBUG>0):
        print( "slice_deg_2_cMpc -> FoV of input slice = ", fov_in_cMpc, ", Output FoV = ", Lout_cMpc )
    Nup = int( np.ceil(Lout_cMpc / fov_in_cMpc) )

    data_tiled = np.tile(data_slice, reps=(Nup, Nup))
    if (DEBUG>0):
        print( "slice_deg_2_cMpc -> Input FoV: %s [deg], Tiling repeats: %d" % (fov_in_cMpc, Nup) )

    Nside1 = data_tiled.shape[0]
    Nside2 = Nside1 * Lout_cMpc / ( fov_in_cMpc*Nup )
    if (DEBUG>0):
        print( "slice_deg_2_cMpc -> Nside1 & Nside2 = ", Nside1, Nside2, (Nside1 - Nside2)/2.0 )

    zoom = (dim_out+0.0) / Nside2  # +0.1 to workaround the scipy warning
    indx_cut = int( np.floor( (Nside1 - Nside2)/2.0 ) )
    if (DEBUG>0):
        print( "slice_deg_2_cMpc -> upsampling with zoom = ", zoom )
        print( "slice_deg_2_cMpc -> before trimming to centre, data shape = ", data_tiled.shape )
    if (cut_ctrl is True): # Return central slice of FoV
        if (indx_cut>0):
            img2_data = data_tiled[indx_cut:-indx_cut, indx_cut:-indx_cut] # cut out the central region with Nside2^2 pixels
        elif (indx_cut<0):
            print("slice_deg_2_cMpc -> Alert, there is something going on with the indexing for data field")
        else:
            img2_data = data_tiled
    else: # Return the bottom left slice
        if (indx_cut>0):
            indx2 = np.int( np.ceil(Nside2) )
            img2_data = data_tiled[0:indx2, 0:indx2]
        elif (indx_cut<0):
            raise ValueError("slice_deg_2_cMpc -> Alert, there is something going on with the indexing for data field")
        else:
            img2_data = data_tiled

    if (DEBUG>0):
        print( "slice_deg_2_cMpc -> After cutting out the central region) = ", img2_data.shape, " Use indx_cut = ", indx_cut)
    # Rescale to the output size
    data_slice = scipy.ndimage.zoom(img2_data, zoom=zoom, order=0, mode='wrap')
    if (DEBUG>0):
        print( "slice_deg_2_cMpc -> after zoom = ", data_slice.shape )
    if not( data_slice.shape[0] == dim_out ):
        idx_ceil = int( np.ceil( (data_slice.shape[0] - dim_out)/2.0 ) )
        idx_floor = int( np.floor( (data_slice.shape[0] - dim_out)/2.0 ) )
        if (DEBUG>0):
            print("slice_deg_2_cMpc -> index ceil & floor = ", idx_ceil, idx_floor)
        data_slice = data_slice[idx_floor:-idx_ceil, idx_floor:-idx_ceil]
    if (DEBUG>0):
        print( "slice_deg_2_cMpc -> after refining pixel trim = ", data_slice.shape )

    return data_slice, img2_data.shape[0]

def lc_cMpc_2_deg(data_in=None, header_dict=None, FoV_deg=None, Nside_deg=None, outfile_prefix=None, freq_lo=None, freq_hi=None, freq_del=None, cut_ctrl=False, write=True,cosmo=None):
    '''
        Uses an adapted version of Weitian LI's (https://github.com/liweitianux/atoolbox.git)
        Lightcone class to resample a lightcone in the frequency direction.

        args:
        data_in (float n*n*m float) - datacube (with freq/z along z-axis/axis-2)
        header_dict (dictionary - can use make_header to create) containing -
            ["Nside"] number pixels on side (for the xy slices)
            ["Dc_step"] step size in comoving Mpc
            ["Dc_min"] comoving distance of highest freq/lowest z slice
            ["Lside"] Simulation side length in cMpc
            ["BUNIT"] unit of data mK or K
        FoV_deg (float) - desired size of output slices in degrees
        Nside_deg (int) - desired number of pixels per side for output slices (in degrees)
        outfile_prefix (string) (opt) - string for saving slices or False to stop module writing out to file
        freq_lo (float) (opt) - lowest freq for output slice
        freq_hi (float) (opt) - highest freq for output slice
        freq_del (float) (opt) - increment of frequency slices
        cut_ctrl (bool) (opt default=False) - returns the bottom left slice by default, set to False if you want to return the central slice
        cosmo (class) (opt) - instance of astropy FlatLambdaCDM (or similar) cosmology class

        Returns:
        lc_obs - lightcone with xy-resolution in constant degree and z-resolution in constant freq
        redshifts - redshifts of each xy slices
        freqs - frequencies of each xy slices
        file_list - file list (empty if outfile_prefix=False)
    '''

    if (cosmo is None):
        cosmo = cvdist.cosmo_default()

    if ( (data_in is None) or (header_dict is None) or (FoV_deg is None) ):
        raise ValueError("convert_lightcones -> lc_cMpc_2_deg. USAGE: lc_cMpc_2_deg(data_in=None, header_dict=None, FoV_deg=None)")

    Nslice = data_in.shape[2]

    #/Users/caw11/_PROGRAMS/OSKAR/atoolbox/astro/21cm/freq2z.py $FREQ_LO:$FREQ_DEL:$FREQ_HI > freqs.list; #| awk '/^1/ { print $1 }' > freqs.list;
    if (freq_hi is None):
        cdist_low = header_dict["Dc_min"]
        if (DEBUG>0):
            print("cdist(low) = ", cdist_low)
        zlow = z_at_value( cosmo.comoving_distance, cdist_low*au.Mpc )
        if (DEBUG>0):
            print("zlow = ", zlow)
        freq_hi = cvdist.nu_from_z( zlow )
        if (DEBUG>0):
            print("freq_hi = ", freq_hi)
    if (freq_lo is None):
        cdist_hi = cdist_low + header_dict["Dc_step"]*(Nslice-1)
        zhi = z_at_value( cosmo.comoving_distance, cdist_hi*au.Mpc )
        if (DEBUG>0):
            print("cdist (high) = ", cdist_hi)
            print( "zhigh = ", zhi )
        freq_lo = cvdist.nu_from_z( zhi )
        if (DEBUG>0):
            print( "freq_lo = ", freq_lo )
    if (freq_del is None):
        freq_del = (freq_hi - freq_lo)/Nslice
        if (DEBUG>0):
            print( "freq_del = ", freq_del )
    freqs = []
    begin, step, stop = freq_hi, -freq_del, freq_lo-freq_del/2.0
    #print( "%.2f  %.2f  %.2f" % (begin, step, stop) )
    #v = np.arange(begin, (stop+step/2.0), step)
    v = np.arange(begin, stop, step)
    '''if ( cvdist.z_from_nu(v[0]) > zhi ):
        print( "convert_lightcones.py (lc_cMpc_2_deg) -> adjusting highest freq to be within box bounds, before nuhi = ", v[0])
        v[0] = cvdist.nu_from_z( zhi - 0.00- )
        print( "convert_lightcones.py (lc_cMpc_2_deg) -> after nuhi = ", v[0], "zhi = ", cvdist.z_from_nu(v[0]) )'''

    freqs += list(v)
    redshifts = get_slice.freq2z(freqs)

    lightcone = get_slice.LightCone(data_in=data_in, header_dict=header_dict, cosmo=cosmo)

    if (outfile_prefix is None):
        outfile_prefix = "deltaTb_"+str(header_dict["BUNIT"])
        write_to_file = True
    elif (outfile_prefix is False):
        write_to_file = False
    else:
        write_to_file = True
    outfile_pattern = "{prefix}_f{freq:06.2f}_z{z:06.3f}.fits"

    if (Nside_deg is None): # set resolution as per lowest z in this box
        print("convert_lightcones.py (lc_cMpc_2_deg) -> User not defined Nside_deg so using minimum redshift = ", np.amin(redshifts), " to conserve resolution of passed datacube")
        cdist = cosmo.comoving_distance( np.amin(redshifts) ).value
        fov_in_deg = (header_dict["Lside"] / cdist) * au.rad.to(au.deg)
        Nside_deg = np.ceil(FoV_deg/fov_in_deg*header_dict["Nside"])
        print("convert_lightcones.py (lc_cMpc_2_deg) -> upscaling factor = ", np.ceil(FoV_deg/fov_in_deg), ", set Nside_deg to ", Nside_deg)
    else:
        if (DEBUG>0):
            print("convert_lightcones.py (lc_cMpc_2_deg) ->  Nside_deg = ", Nside_deg)

    lc_obs=np.zeros( (Nside_deg, Nside_deg, len(freqs)) )
    lc_cMpc=np.zeros( (data_in.shape[0], data_in.shape[1], len(freqs)) )
    print(lc_obs.shape, lc_cMpc.shape)
    file_list = []
    ct=0
    for z, f in zip(redshifts, freqs):
        slice_cMpc = lightcone.get_slice(z)
        if (DEBUG>0):
            print("convert_lightcones.py (lc_cMpc_2_deg) -> pixel = %d, z=%06.3f, freq=%06.2f MHz..." % (ct, z, f))
        slice_deg, dim_trim = slice_cMpc_2_deg( data_slice=slice_cMpc, Lin_cMpc=header_dict["Lside"], z=z, cut_ctrl=cut_ctrl, Lout_deg=FoV_deg, dim_out=Nside_deg, cosmo=cosmo )

        header_deg = {}
        header_deg["FoV_deg"] = FoV_deg
        header_deg["Nside"] = Nside_deg
        if (write_to_file is True):
            outfile = outfile_pattern.format(prefix=outfile_prefix, z=z, freq=f)
            if (DEBUG>0):
                print("convert_lightcones.py (lc_cMpc_2_deg) -> writing to outfile = : %s ..." % (outfile))
            lightcone.write_slice( outfile, data=slice_deg, z=z, clobber=True, hdr_deg=header_deg )
            file_list.append(outfile)

        lc_obs[:,:,ct] = slice_deg
        lc_cMpc[:,:,ct] = slice_cMpc
        ct+=1

    #return lc_obs, lc_cMpc, redshifts, freqs, file_list # Use if wanna debug, also returns cMpc coeval resampled to constant freq bins
    return lc_obs, redshifts, freqs, file_list

def lc_deg_2_cMpc(data_in=None, FoV_deg=None, L_cMpc=None, Nside_cMpc=None, redshifts=None, cut_ctrl=False, cosmo=None):
    '''
        Uses an adapted version of Weitian LI's (https://github.com/liweitianux/atoolbox.git)
        Lightcone class to resample a lightcone in the frequency direction.

        args:
        data_in (float n*n*m float) - datacube (with freq/z along z-axis)
        FoV_deg (float) - desired xy sidelength of input cube in degrees
        L_cMpc (float) - desired xy sidelength of output cube in cMpc
        Nside_CMpc (int) - desired number of pixels per side for output slices (in cMpc)
        redshifts (list)
        cosmo (class) (opt) - instance of astropy FlatLambdaCDM (or similar) cosmology class
    '''

    if (cosmo is None):
        cosmo = cvdist.cosmo_default()

    if ( (data_in is None) or (FoV_deg is None) or (L_cMpc is None) or (Nside_cMpc is None) ):
        raise ValueError("convert_lightcones -> lc_deg_2_cMpc. USAGE: lc_deg_2_cMpc(data_in=None, FoV_deg=None, FoV_cMpc=None, Nside_cMpc=None)")

    datacube_cMpc=np.zeros( (Nside_cMpc, Nside_cMpc, data_in.shape[2]) )
    ct=0
    for z_slice in redshifts:
        if (DEBUG>0):
            print(z_slice)
        dataslice_cMpc, dim_trim = slice_deg_2_cMpc( data_slice=data_in[:,:,ct], Lin_deg=FoV_deg, z=z_slice, cut_ctrl=cut_ctrl, Lout_cMpc=L_cMpc, dim_out=Nside_cMpc, cosmo=cosmo )
        datacube_cMpc[:,:,ct] = dataslice_cMpc
        ct+=1

    return datacube_cMpc
