'''
	simulate_instrumentals.py

	This module simulates instrumental uv-samling to generate the following:
	1. applies just uv-sampling to a simulation
	2. applies uv-sampling and adds instrumental noise as Gaussian random over uv-samples
	3. in addition to 2. adds sample variance as random noise (but this is a terrible approximation and I don't reccomend using)
	4. returns a uv-noise maskfile

	The beauty of this is that it is all done in simulation co-ordinates and on the
	first iteration makes a maskfile with the dimensions of the user defined HII_DIM and LEN
	which makes it super fast.

	Suggested applications include adding in uv-sampling to an MCMC pipeline such as 21cmMC,
	generating a simulated dataset (with uv-sampling + noise applied)
	or estimating the error on a statistic using monte-carlo
	sampling (naturally requires the calculation time of your statistic to be fast
	enough to MC)

	See README for info on usage
'''
from scipy.stats import skew

import os, sys
import numpy as np
import numpy.random as random
import math
from scipy import interpolate
from decimal import *
import h5py
import astropy.io.fits as pyfits
from subprocess import call
from pathlib import Path
from scipy import interpolate
from decimal import *

from numpy.fft import fftn, ifftn, fftfreq, rfftfreq

from astropy.cosmology import FlatLambdaCDM
import astropy.units as au
from astropy.convolution import convolve, convolve_fft
from astropy.convolution import Gaussian2DKernel

from . import convs_dists as conv

np.seterr(invalid='ignore', divide='ignore')

DEBUG=0 # 0 Supresses unnecessary print outs, 1 = print basic runtime output, 2 = debug mode, print everything out.

TWOPLACES=Decimal(10)**-2 # USAGE: Decimal(your_variable).quantize(TWOPLACES)

''' ------------------------------------------------------------------------------------------------------------

									 'HELPER' FUNCTIONS

		  USED BY MAIN 'ACTION' FUNCTIONS, USER SHOULD NOT NEED TO CALL THESE DIRECTLY

 ------------------------------------------------------------------------------------------------------------ '''

def find_closest_1Dindex(k=None, kf=None):
	#
	cont_bin = (k+0.0)/(kf+0.0)
	#
	floor_and_ceil = [ math.floor(cont_bin), math.ceil(cont_bin) ]
	ind =  floor_and_ceil[( np.abs(floor_and_ceil-cont_bin) ).argmin()]
	'''if ( abs(k-0.58450692) <= 0.00001 ):
		print("find_closest_1Dindex() ->  k = ", k, "kf = ", kf)
		print("find_closest_1Dindex() ->  continuous bin = ", cont_bin)
		print("find_closest_1Dindex() ->  floor_and_ceil = ", floor_and_ceil)
		print("find_closest_1Dindex() ->  index assigned to sample = ", ind)'''

	return ind

def load_ks_Pnoise(Filepath=None):
	if (Filepath is None):
		print("load_ks_Pnoise() -> Problem: I can't very well open fresh air  \n USAGE: load_ks_Pnoise(Filepath)")
		print -1
	else:
		'''if (DEBUG >= 1):
			print("load_ks_Pnoise() -> loading data from h5 file")'''
		# Open and load kx, ky, kz and Pnoise
		with h5py.File(Filepath, 'r') as hf:
			kx = hf['kx_cMpc'][:]
			ky = hf['ky_cMpc'][:]
			kz = hf['klos_cMpc'][:]
			Pnoise = hf['Pnoise'][:]
	'''if (DEBUG >= 2):
		print( "Length of imported k and Pnoise arrays = ", len(kx) )'''
	return kx, ky, kz, Pnoise
'''
def k_pls(z=None, Dim=None, Len=None, omega_m=0.3075, hlittle=0.6774):
	'
		Returns the k_pls associated with the freq Len and Dim of the frequency axis
		z is the middle redshift associated with the centre of the freq range
	'
	if  ( (z is None) or (Dim is None) or (Len is None) or  (z is None) ):
		print("k_pls-> You need to pass at least z, Dim, and Len arguments")
		return -1

	#Multiply by this to convert a bandwidth in MHz to a line of sight distance in Mpc/h at redshift z
	# [h^-1 Mpc]/MHz, from Furlanetto et al. (2006)

	#dL_df = (1.7 / 0.1) * ((1+z) / 10.)**.5 * (omega_m/0.15)**-0.5 #* 1e3 # converted from original to /MHZ

	# Multiply by this to convert eta (FT of freq.; in 1/GHz) to line of sight k mode in 1/Mpc at redshift z
	# 2pi * [h Mpc^-1] / [MHz^-1]
	# dk_deta = 2*np.pi*hlittle / dL_df
	B = conv.delnu_from_cMpc(cMpc=Len, z_mid=z, omega_m, hlittle) # MHZ
	# replaced the following 21cmSENSE approx conversion (after testing)
	#eta_los = dk_deta*np.fft.fftfreq(Dim, B/Dim)
	eta_los = np.fft.fftfreq(Dim, B/Dim)
	kpls=np.zeros( len(eta_los) )
	for i in range( len(eta_los) ):
		kpls[i] = ( conv.klos_from_eta( eta_los[i], z, omega_m, hlittle ) )

	return kpls #eta_los
'''
def random_sample_noise(Pnoise, noise_seed=None):
	'''
		This generates a 3D fft_coeval with random sampling of noise assuming mean zero and sigma = sqrt(Pnoise)
		Pnoise.real = Power spectrum of instrumental noise
		Pnoise.complex = Power spectrum of sample variance (equivalent to the power spec of the Tb signal on a given scale)
	'''
	noise_1sig_samp = np.zeros(Pnoise.shape, dtype=complex)

	random.seed(noise_seed) # This uses the time as a seed, so will produce a different pattern of numbers each time
	rng_real = random.normal(0.0, np.array(np.sqrt( np.real(Pnoise) ), dtype=np.float32))
	#print( np.mean(rng_real), np.var(rng_real), skew(rng_real, axis=None) )
	random.seed(noise_seed) # I want a fresh seed for the complex noise samples
	rng_img = random.normal(0.0, np.array(np.sqrt( np.real(Pnoise) ), dtype=np.float32))

	#print( np.mean(rng_img), np.var(rng_img), skew(rng_img, axis=None) )
	for ind, value in np.ndenumerate(Pnoise):
		#rng_real = random.normal( 0.0, np.sqrt( np.real(value) ) )
		#rng_img = random.normal( 0.0,  np.sqrt( np.real(value) ) )
		#noise_1sig_samp[ ind ] = complex( rng_real, rng_img )
		noise_1sig_samp[ ind ] = complex( rng_real[ind], rng_img[ind] )

		'''if ( (ind[0] < 10) and (ind[1] < 10) and (ind[2] < 10) ):
			print( "In RNG: ", rng_real[ind], rng_img[ind], value)
			print( noise_1sig_samp[ ind ])'''

		# cw check next line is right, I think not and should be replaced by the following
		# note neither are needed as the noise does not have to obey rules of real data
		# -> it is random noise on complex data
		#noise_1sig_samp[ ind[0], ind[1], Pnoise.shape[2]-ind[2] ] = np.conj( noise_1sig_samp[ ind[0], ind[1], ind[2] ] )
		#noise_1sig_samp[ ind[0], ind[1], Pnoise.shape[2]-ind[2] ] = complex( random.normal( 0.0, ( np.sqrt(value.real)+np.sqrt(value.imag) ) ), random.normal(0.0, (np.sqrt(value.real)+np.sqrt(value.imag)) ) )

	'''if (DEBUG >= 2):
		print("random_sample_noise -> mean, variance = ", np.mean( noise_1sig_samp ), np.var( noise_1sig_samp ) )'''

	return noise_1sig_samp

def generate_uv_mask(kx=None, ky=None, kz=None, Pnoise=None, Psv=None, Dim=None, Len=None, redshft=None, omega_m=0.3075, hlittle=0.6774):
	'''
	This generates a 1 if the pixel is sampled by the passed kx, ky, kz and a 0 if not
	INPUTS
	Required:
	[kx], [ky], [kz] (cMpc^{-1}) previously loaded from uv sampling/noise file and
	Optional:
	[sim_fft_data] - FFTed simulation
	[Pnoise] - sim_fft_data.shape array
	[Psv] - interpolation fn of sample variance power (which is the PS of cosmo Tb signal)
		(see outputs for how these control behaviour)

	OUTPUTS
	[mask_fft_coeval] which is a coeval (real but indexed as fft box)
	with zeroes where no corresponding uv samples, otherwise:

	[Pnoise is None] only - Total number of uv samples in each sampled pixel
	not[Pnoise is None] - The INSTRUMENTAL NOISe power in each sampled pixel for use with random_sample_noise (**stored in real part of mask)
	not[Psv is None] - contribution to noise from sample variance included (**stored in complex part of mask)
	'''
	if ( (kx is None) or (ky is None) or (kz is None) or (Dim is None) or (Len is None) or (redshft is None) ):
		print("generate_uv_mask() -> Come on dude, give me something to work with! \n USAGE: generate_uv_mask(kx, ky, kz, Dim, Len, z, omega_m, hlittle)")
		return -1

	if not(Psv is None):
		print("generate_uv_mask() -> !!!! ---- IMPORTANT ----!!!! -> Treating the sample variance as a random error like this is just incorrect, so use this option with caution")
	# Calculate fundamental k-pixel size on the sky i.e. k_perp
	kf = 2.0*np.pi/(Len+0.0)

	# Work out the k_eta modes in the frequency direction (i.e los/parallel) to calculate fundamental k_eta pixel size
	#kpls = k_pls(z=redshft, Dim=Dim, Len=Len, omega_m=omega_m, hlittle=hlittle)
	#kf_eta = kpls[1] - kpls[0]
	#if (DEBUG >= 1):
	#	print("generate_uv_mask() -> Fundamental k-pixel size for freq axis is ", kf_eta, "\n kpls = ", kf_eta)

	if not((Dim % 2) == 0): # To allow for fourier padding
		'''if (DEBUG >= 1):
			print("generate_uv_mask() -> padding mask as array dimension is not even" )'''
		Dim+=1
	'''if (DEBUG >= 1):
		print("generate_uv_mask() -> Dim = ", Dim )'''
	mask_fft_coeval = np.zeros( (Dim, Dim, Dim), dtype=complex )
	samp_ct = np.zeros( (Dim, Dim, Dim) )
	#print(mask_fft_coeval.shape)
	for i, k_x in enumerate(kx):
		ind_x = find_closest_1Dindex( k=k_x, kf=kf )
		ind_y = find_closest_1Dindex( k=ky[i], kf=kf )
		ind_z = find_closest_1Dindex( k=kz[i], kf=kf )

		# Ignore anything that is larger than the largest k-scale in our box.
		if ( ( np.abs(ind_x) < Dim/2.0 ) and  ( (np.abs(ind_y) < Dim/2.0) ) and (np.abs(ind_z) < Dim/2.0) and not( (ind_x == 0) and (ind_y == 0) and (ind_z== 0) ) ): # don't sample 0th k mode.
			#print(ind_x, ind_y, ind_z, Pnoise[i])
			if (Pnoise is None): #  Generating a basic uv sampling mask as no noise has been passed
				mask_fft_coeval[ind_x, ind_y, ind_z] += 1.0
			else: # Generating a noisey uv sampling mask by adding in inverse quadrature
				#calculate the kmag for this sample (for sample variance)
				kmag = np.sqrt(k_x*k_x + ky[i]*ky[i] + kz[i]*kz[i])
				'''
				if not(Psv is None): #cw I am removing this functionality as it is inconsistent with sv
					if (DEBUG >= 3):
						print("generate_coeval_uv_mask() -> kmag = ", kmag, "Pnoise = ", Pnoise[i], "Psv = ", Psv(kmag) )
					# Note that treating the sample variance as a random error like this is just incorrect, so use this option with caution
					mask_fft_coeval[ind_x, ind_y, ind_z] += complex( 1.0/Pnoise[i], Psv(kmag) ) # IVW the noise, averaging the
				else:
				'''
				'''if (DEBUG >= 3):
					print("generate_coeval_uv_mask() -> kmag = ", kmag, "Pnoise = ", Pnoise[i] )'''
				mask_fft_coeval[ind_x, ind_y, ind_z] += complex( 1.0/Pnoise[i], 0.0 )
				samp_ct[ind_x, ind_y, ind_z] += 1.0
				#print("generate_coeval_uv_mask() -> ", samp_ct[ind_x, ind_y, ind_z], mask_fft_coeval[ind_x, ind_y, ind_z] )

	if (Pnoise is None): # Just return the mask with number of uv samples per pixel
		return mask_fft_coeval
	else: # invert ivw power in each sampled cell and for the sample variance divide by number of independent samples
		for ind, ct in np.ndenumerate(mask_fft_coeval):
			if (samp_ct[ind]>0.0):
				#print("Before = ", mask_fft_coeval[ind], "ind = ", ind)
				mask_fft_coeval[ind] = complex( (1.0/mask_fft_coeval[ind].real), (mask_fft_coeval[ind].imag/samp_ct[ind]) ) #/samp_ct[ind] # inverting as added error in inverse quad and dividing by number of independent samples
				#print("After (real) = ", mask_fft_coeval[ind].real)
				#print("After (imag) = ", mask_fft_coeval[ind].imag)
		return mask_fft_coeval


''' ------------------------------------------------------------------------------------------------------------

										  'ACTION' FUNCTIONS

				USE DIRECTLY THESE TO GENERATE UV-SAMPLING MASKS (WITH OR WTHOUT NOISE)
				AND TO CREATE RANDOM REALISATIONS OF OBSERVATION NOISE + UV-SAMPLE A PASSED SIMULATION

 ------------------------------------------------------------------------------------------------------------ '''

def print_cmSENSE_flags(UParams=None, CParams=None, z=None):
	'''
		Given as set of 21mMC user-settings dictionaries, suggests appropriate flags for use with uv_to_k_plus_calc_sense.py
		which can be used with the array files generated by 21cmSENSE mk_array_file.
		(this provides kx, ky, kz, Pnoise files for use in the rest of this package)
	'''
	if (UParams is None) or (CParams is None) or (z is None):
		print('print_cmSENSE_flags() -> Give a dog a bone! \n Usage reminder :\n print_cmSENSE_flags(UParams, CParams, z)')
		return -1
	print('print_cmSENSE_flags() -> The following provides the bandwidth and nchan flags to pass to uv_to_k_plus_calc_sense.py to generate a uv-sampling/noise file for current parameter settings')
	print('z = ', z)
	print('--nchan ', UParams.HII_DIM, '--boxlen', UParams.BOX_LEN, ' --bwidth ', conv.delnu_from_cMpc(UParams.BOX_LEN, z, CParams.OMm, CParams.hlittle)/1000.0,  ' --hlittle ', CParams.hlittle,  ' --omega_m ', CParams.OMm)
	return

def get_obs_fft_coeval(kx=None, ky=None, kz=None, Pnoise=None, Psv=None, Dim=None, Len=None, uv_mask=None, sim_noise=False, sim_fft_data=None, redshft=None, omega_m=0.3075, hlittle=0.6774, noise_seed=None):
	'''
		Use this to generate a uv-sampling fft mask (with or without noise 1-sigma in sampled pixel),
		or sample and/or add noise to a simulation ffted coeval

		INPUTS
		Given [kx], [ky], [kz] (cMpc^{-1}) previously loaded from uv sampling/noise file
		[sim_fft_data], [uv_mask] and [Pnoise] optionals (see outputs for how these control behaviour)
		[noise_seed] seed for random noise generation, default: None (a new realisation each time)

		OUTPUTS
		[obs_fft_coeval] which is:

		dT-only uv-sampled fft coeval - [not(sim_fft_data is None)] and [Pnoise=None]

		noise-only uv-sampled fft coeval - [sim_fft_data is None] and [not(Pnoise is None)]

		dT+noise uv-sampled fft coeval - [not(sim_fft_data is None)] and [not(Pnoise is None)]

		sampling mask fft coeval - [uv_mask is None] (0 where not sampled and Num_samps elsewhere if [sim_noise=False] and noise_1sigma if [sim_noise=True])

		When trying to generate multiple noisey observations of the same simulation.
		Call this once with [uv_mask=None] then pass the returned noise mask for all future calls.

	'''

	if ( (kx is None) or (ky is None) or (kz is None) or (redshft is None)) :
		print("get_obs_fft_coeval() -> You have failed to pass kx, ky and kz arrays.\n !!!!! ABORTING perform_uv_sampling() without performing sampling.")
		return -1
	if not( len(kx)==len(ky)==len(kz) ):
		print("get_obs_fft_coeval() -> There is something fishy here, the lengths of kx (", str(len(kx)),"), ky (", str(len(ky)),"), and kz (", str(len(kz)) ," are not the same. \n !!!!! ABORTING perform_uv_sampling() without performing sampling.")
		return -1
	elif not(Pnoise is None):
		if not( len(kz)==len(Pnoise) ):
			print("get_obs_fft_coeval() -> There is something fishy here, the lengths of kx (", str(len(kx)),"), ky (", str(len(ky)),"), kz (", str(len(kz)) ,") and Pnoise (",str(len(Pnoise)) ,") are not the same. \n !!!!! ABORTING perform_uv_sampling() without performing sampling.")
			return -1

	# If no uv mask is passed just return a sampling mask (0 if a uv pixel, 1 otherwise) designed for use with numpy.ma
	if ( uv_mask is None ):
		'''if (DEBUG >= 2):
			print( "get_obs_fft_coeval -> uv_mask average in get_obs_fft_coeval() = ", uv_mask )
		if (DEBUG >= 1):
			print("get_obs_fft_coeval() -> Generating a coeval noise mask. P_noise where sampled, zero otherwise.")'''
		obs_coeval = generate_uv_mask( kx=kx, ky=ky, kz=kz, Pnoise=Pnoise, Psv=Psv, Dim=Dim, Len=Len, redshft=redshft, omega_m=omega_m, hlittle=hlittle )
		'''if (DEBUG >= 2):
			hdu = pyfits.PrimaryHDU(  np.real( np.fft.fftshift(obs_coeval) )  )
			hdulist = pyfits.HDUList([hdu])
			filename = "Pnoise_mask_fft_z"+str(redshft)+".fits"
			call(["rm", filename])
			hdulist.writeto(filename)
			hdulist.close()'''
		return obs_coeval

	# If only a simulated fft coeval is passed, just apply uv sampling and return
	elif ( (sim_noise is False) and not(sim_fft_data is None) ):
		'''
		if (DEBUG >= 1):
			print( "get_obs_fft_coeval() -> Generating a observed dT coeval (no noise) as only sim_fft_data has been passed" )'''

		sim_fft_data[ (uv_mask==0j) ] = 0j
		return sim_fft_data

	# If sim_Noise is True then applying sampling and simulate the noise
	elif ( (sim_noise is True) and not(sim_fft_data is None) ):
		'''
		if (DEBUG >= 1):
			print("get_obs_fft_coeval() -> Applying UV sampling and adding random noise to the simulation passed")
			print( "uv_mask shape = ", uv_mask.shape )'''
		sim_fft_data[ (uv_mask==0j) ] = 0j
		noise_sampling = random_sample_noise( Pnoise=uv_mask, noise_seed=noise_seed )
		'''if (DEBUG >= 2):
			hdu = pyfits.PrimaryHDU(  np.real( np.fft.fftshift(noise_sampling) )  )
			hdulist = pyfits.HDUList([hdu])
			filename = "noise_fft_z"+str(redshft)+".fits"
			call(["rm", filename])
			hdulist.writeto(filename)
			hdulist.close()'''
		'''for ind, value in np.ndenumerate(noise_sampling):
			sim_fft_data[ ind ] += noise_sampling[ ind ]'''  # This operation was massively slow compared with doing the next line
		sim_fft_data = sim_fft_data + noise_sampling

		return sim_fft_data
	else: # No simulation passed, so just make a noisy box
		'''if (DEBUG >= 1):
			print("get_obs_fft_coeval() -> no simulation passed, returning noisy box")'''
		noise_sampling = random_sample_noise( Pnoise=uv_mask, noise_seed=noise_seed )
		'''if (DEBUG >= 2):
			print("get_obs_fft_coeval() -> noise sampling", np.fft.fftshift(noise_sampling)[20,20,20], np.fft.fftshift(noise_sampling)[30,30,30])
			hdu = pyfits.PrimaryHDU(  np.real( np.fft.fftshift(noise_sampling) )  )
			hdulist = pyfits.HDUList([hdu])
			filename = "noise_fft_z"+str(redshft)+".fits"
			call(["rm", filename])
			hdulist.writeto(filename)
			hdulist.close()'''
		return noise_sampling

def get_obs_coeval(Filepath=None, z=None,  Dim=None, Len=None, sim_noise=False, Psv=None, sim_coeval=None, omega_m=0.3075, hlittle=0.6774, noise_seed=None):
	'''
		This generates a real-space noisy box using the uv-sampling and noise power spec
		(see README.md for the required format of uv-sampling/noise file).
		It does not allow for line-of-sight evolution in the uv sampling and noise so only to be applied over small bandwidth.

		The simulation is also assumed to be coeval in cMpc^3.
		Units of returned field are mK.
		The workflow in this can be used as a template for generating multiple noise realisations.

		ARGS:
		[Filepath] path to uv-sampling/noise file
		[Dim] number pixels on the box side
		[Len] Size of coeval box side in cMpc
		[sim_noise] true it simulates noise
		[sim_coeval] simulated dT coeval
		[noise_seed] seed for random noise generation, default: None (a new realisation each time)

		RETURNS:
		[noise_box] which is an observed [sim_coeval]+noise observation coeval...
					or without noise if [sim_noise=False]...
					or just a noisy box if [sim_noise=True] and [sim_coeval is None]..
					or just the fft mask if [sim_noise=False] and [sim_coeval is None]..
	'''
	if ( (Filepath is None) or (Filepath is -1) or (z is None) or (Dim is None)  or (Len is None) ):
		print("get_obs_coeval() -> Use this as a module to add uv sampling and/or noise to your simulation, or if you don't pass a simulation just generates a uv-noise mask. USAGE (excluding optionals): get_obs_coeval(Filepath=None, z=None,  Dim=None, Len=None)... !!!ABORTING!!!")
	else:
		pixel_size = Len/(Dim+0.0)
		'''if (DEBUG >= 1):
			print("get_obs_coeval() -> pixel size = ", pixel_size)'''

		kx, ky, kz, Pnoise = load_ks_Pnoise(Filepath=Filepath)

		# First we generate a noise mask, this is all that happens if no simulation has been passed, this has P_noise in pixels which have been samples
		# Check if we already have a mask on disk for the corresponding noise file and use instead of doubling effort
		if (Psv is None):
			mask_fname=Filepath[0:-3]+"inclSV0_maskfile.h5"
			#mask_fname="inclSV0_maskfile.h5"
		else:
			mask_fname=Filepath[0:-3]+"inclSV1_maskfile.h5"
			#mask_fname="inclSV1_maskfile.h5"
		mask_fpath = Path(mask_fname)
		if mask_fpath.exists():
			'''if (DEBUG >= 1):
				print("get_obs_coeval() -> ", mask_fname, " mask_file exists, no need to call gen_mask via get_obs_fft_coeval")'''
			with h5py.File(mask_fname, 'r') as hf:
				mask_for_ffted_coeval = hf['mask_sampling_box'][:]
			'''if (DEBUG >= 1):
				print("get_obs_coeval() -> ", mask_fname, " completed read in of maskfile")'''
		else:
			'''if (DEBUG >= 1):
				print("get_obs_coeval() -> ", mask_fname, " mask_file does not exist, so generating from passed uv_noise_file from scratch")'''
			mask_for_ffted_coeval = get_obs_fft_coeval( kx=kx, ky=ky, kz=kz, Pnoise=Pnoise, Psv=Psv, Dim=Dim, Len=Len, redshft=z, omega_m=omega_m, hlittle=hlittle, uv_mask=None, noise_seed=noise_seed  )
			with h5py.File(mask_fname, 'w') as hf:
				hf.create_dataset( 'mask_sampling_box', data=mask_for_ffted_coeval )

		if ( not(sim_coeval is None) ): # a dT-sim coeval has been passed
			'''if (DEBUG >= 1):
				print("get_obs_coeval() -> 21cmMC simulation has been passed")
				print("get_obs_coeval() -> FFTing simulation real-space 2 k-space")'''
			#cw sim_fft_coeval, ks_sampling = conv.fft_data(sim_coeval, L=Len) # this fft doesn't work
			sim_fft_coeval = fftn(sim_coeval)
			'''if (DEBUG >= 2):
				print("get_obs_coeval() -> Data shape after FFT = ", sim_fft_coeval.shape)'''
			if ( sim_noise is False ): # then please don't simulate noise, simply apply uv sampling
				'''if (DEBUG >= 1):
					print("get_obs_coeval() -> Applying uv-sampling, but not adding noise")'''
				obs_coeval_fft = get_obs_fft_coeval( kx=kx, ky=ky, kz=kz, Dim=Dim, Len=Len, uv_mask=mask_for_ffted_coeval, sim_noise=False, sim_fft_data=sim_fft_coeval, redshft=z, omega_m=omega_m, hlittle=hlittle, noise_seed=noise_seed )
			else: # observed dT+noise coeval
				'''if (DEBUG >= 1):
					print("get_obs_coeval() -> Applying uv-sampling and adding noise")'''
				obs_coeval_fft = get_obs_fft_coeval( kx=kx, ky=ky, kz=kz, Dim=Dim, Len=Len, uv_mask=mask_for_ffted_coeval, sim_noise=True, sim_fft_data=sim_fft_coeval, redshft=z, omega_m=omega_m, hlittle=hlittle, noise_seed=noise_seed )
			'''if (DEBUG >= 1):
				print("get_obs_coeval() -> FFTing simulation k-space 2 real-space")'''
			#obs_coeval, x_samp = conv.ifft_data( obs_coeval_fft, L=Len ) # this fft method doesn't work
			obs_coeval = ifftn( obs_coeval_fft )
			#if not( x_samp[1][1] == Len/(Dim+0.0) ) :
			#	print("get_obs_coeval() -> Oh my, it seems like there is something wrong with the FFT scalings. x_sampl = ", x_samp[1][1], "should be Len/Dim = ", Len/(Dim+0.0))

		# FFT c2r and make absolute (as we are always dealing with real data here)
		else: #no simulation was passed so either return the mask or a noisy box
			if ( sim_noise is False ): # noise-only coeval (basically the noisy mask)
				'''if (DEBUG >= 1):
					print("get_obs_coeval() -> no simulation has been passed...")
					print("get_obs_coeval() -> Not FFTing k-space 2 real-space as just returning the mask")'''
				return mask_for_ffted_coeval
			else:
				'''if (DEBUG >= 1):
					print("get_obs_coeval() -> generating a noisy box")'''
				obs_coeval_fft = get_obs_fft_coeval( kx=kx, ky=ky, kz=kz, Dim=Dim, Len=Len, uv_mask=mask_for_ffted_coeval, sim_noise=True, sim_fft_data=None, redshft=z, omega_m=omega_m, hlittle=hlittle, noise_seed=noise_seed )
				'''if (DEBUG >= 1):
					print("get_obs_coeval() -> noise sampling", np.fft.fftshift(obs_coeval_fft)[20,20,20], np.fft.fftshift(obs_coeval_fft	)[30,30,30])
					print("get_obs_coeval() -> FFTing noise realisation k-space 2 real-space")'''

				#obs_coeval, x_samp = conv.ifft_data( obs_coeval_fft, L=Len ) # this fft method doesn't work
				obs_coeval = ifftn( obs_coeval_fft )

				'''if (DEBUG >= 2):
					print( obs_coeval.shape, obs_coeval.dtype, np.mean(obs_coeval) )'''

		#return the real part of the box
		return obs_coeval #.astype(np.float32) #because we are working with real space data

'''
	The main method of PyObs21 is currently designed to operate in cMpc directly on simulation coeval,
	we therefore include some simple smoothing modules to apply basic instrumentals to observational maps
'''

def instParams(inst):
    '''
    Returns a dictionary containing the appropriate instrumental properties for
    MWA (inst=0), LOFAR (inst=1) and SKA (inst=2). This dictionary will be used to

    dict['type'] - Name of instrument
    dict['Nstat'] = number of tile (e.g. MWA) or stations (e.g. SKA)
    dict['Aeff'] = effective area of tile/station (m^2) (Acoll/Nstat)
    dict['Tint'] = total integration time (hours)
    dict['Dmax'] = largest tile/station separation (m)
    '''

    if inst==0:
        # MWA params, table 1 from arXiv:1206.6945 (TGB2012) @ 150MHz
        dict={'type':'MWA', 'Nstat':128, 'Aeff':21.5, 'Tint': 1000.0, 'Dmax':2864.0, 'full':0.8, 'r':-2, 'B':8}
    if inst==1:
        #LOFAR params, table from arXiv:1210.0197 (MK2012)
        dict={'type':'LOFAR','Nstat':48, 'Aeff':804, 'Tint':1000.0 , 'Dmax':3000.0, 'full':0.8, 'r':-2, 'B':8}
        #dict={'Nstat':64.0, 'Aeff':7.2e4/64.0, 'Tint':1000.0 , 'Dmax':3000.0}#McQuinn+ 2006
    if inst==2:
        #HERA params, arXiv:1310.7031 (Pober+ 2013); adjusted to 331 to agree with private comms with Pober and Liu
        dict={'type':'HERA','Nstat':331, 'Aeff':84000/331.0, 'Tint': 1000.0, 'Dmax':360.0, 'full':0.8, 'r':-2, 'B':8}
    if inst==3:
        #SKA params from SKA phase 1 baseline design document, with Dmax = 2km.
        dict={'type':'SKA','Nstat':866, 'Aeff':(3.142*(35.0/2.0)**2), 'Tint': 1000.0, 'Dmax':2.0e3, 'full':0.8, 'r':-2, 'B':8}

    return dict

def ang_res_deg(z, inst=None):
	'''
	Angular resolution (degrees) of instrument. Based on eqn in section 9.1 of Furlanetto 2006 review

	INPUTS:
	z (float) - redshift
	inst (dict) (opt) - instParams(inst) like dictionary describing instrument parameters. default instParams(2), i.e. SKA
	'''

	if (z is None):
		raise ValueError("simulate_instrumentals.py (ang_res_deg). Usage: ang_res_degs(z).")

	if (inst is None):
		inst = instParams(2) # defaults to SKA

	Dmax = inst['Dmax']
	res_deg = 1.2*( (1.0+z)/10.0 )*( 100.0/Dmax )

	return res_deg

def ang_res_amins(z=None, inst=None):
	'''
	Angular resolution (arcmins) of instrument. Based on eqn in section 9.1 of Furlanetto 2006 review

	INPUTS:
	z (float) - redshift
	inst (dict) (opt) - instParams(inst) like dictionary describing instrument parameters. default instParams(2), i.e. SKA
	'''
	if (z is None):
		raise ValueError("simulate_instrumentals.py (ang_res_amins). Usage: ang_res_amins(z).")

	res_amins = 60.0*ang_res_deg(z, inst)

	return res_amins

def smooth2resolution(image2D, nu, pixel_deg, inst=None):
	'''
		Smooths the passed 2D image to the resolution of the passed instrument (SKA if None passed)
		at redshift z(nu)

		INPUTS:
		image2D (n x m array) - 2D image
		nu (float) - frequency of image slice in MHz
		pixel_deg (float) - size of pixel in degrees
		inst (dict) (opt) - instParams(inst) like dictionary describing instrument parameters. default instParams(2), i.e. SKA

		RETURNS:
		(n x m array) - smoothed 2D image
	'''

	if ( (image2D is None) or (nu is None) or (pixel_deg is None) ):
		raise ValueError("simulate_instrumentals.py (smooth2resolution). Usage: smooth2resolution(image2D, nu, pixel_deg).")

	if (inst is None):
		inst = instParams(2) # defaults to SKA

	z = conv.z_from_nu(nu)
	res_deg = ang_res_deg(z, inst)

	# Set sigma = number of pixels that corresponds to that resolution/2
	res_in_pxls = res_deg/pixel_deg # standard deviation in number of xy pixels
	'''if (DEBUG>0):
		print( "simulate_instrumentals.py (smooth2resolution). z = ", z, ", nu = ", nu, ", angular res (deg) = ", res_deg, ", number pixels corresponding to resoltion = ", res_in_pxls)'''
	# make Gaussian 2D kernel
	kernel = Gaussian2DKernel(x_stddev=res_in_pxls/2.0)

	# convolve
	image_conv = convolve(image2D, kernel)

	return image_conv

def smooth_datacuboid(dataset, nu_list, pixel_deg, inst=None):
	'''
		Smooths the passed 3D datacube to the resolution of the passed instrument (SKA if None passed)
		at the redshift of each slice which is calculated from the nu_list. z/nu-axis should be axis-2 or z-axis.

		INPUTS:
		dataset (n x m x l array) - 3D image cuboid of slices at l different frequencies, i.e. xy = deg^2, z = redshift or frequency
		nu_list (float list) - frequencies of each image slice (moving in z-direction) in MHz
		pixel_deg (float) - size of pixel in degrees
		inst (dict) (opt) - instParams(inst) like dictionary describing instrument parameters. default instParams(2), i.e. SKA

		RETURNS:
		(n x m array) - smoothed 2D image
	'''

	if ( (dataset is None) or (nu_list is None) or (pixel_deg is None) ):
		raise ValueError("simulate_instrumentals.py (smooth_datacuboid). Usage: smooth_datacuboid(dataset, nu_list, pixel_deg).")

	if (inst is None):
		inst = instParams(2) # defaults to SKA

	smth_dataset = np.zeros_like( dataset )
	for i in range(dataset.shape[2]):
		smth_dataset[:,:,i] = smooth2resolution( dataset[:,:,i], nu_list[i], pixel_deg, inst )

	return(smth_dataset)

def noise_stdev_mK(z, inst=None, SpecRes=0.04, omega_m=0.3075, hlittle=0.6774):

	'''
		Approximation of noise standard deviation in mK based on eqn 141 in Furlanetto 2006 review

		INPUTS:
		z (float) - redshift
		inst (dict) (opt) - instParams(inst) like dictionary describing instrument parameters. default instParams(2), i.e. SKA
		SpecRes (float) (opt) - spectral bandwidth (MHz) over which noise has been integrated over. Default: 0.04MHz
		omega_m, hlittle (float) (opt) - cosmology. Default 0.3075, 0.6774

	'''
	if (inst is None):
		inst = instParams[2] # defaults to SKA

	H_0 = hlittle*100.0
	ang_res_amin = ang_res_amins(z, inst)
	SpecRes = SpecRes*(c*sqrt(1.0+z))/(H_0*1420.40575177*sqrt(omega_m))
	Tint = inst['Tint']

	noise_std_mK = 2.9*(1e5/Atot)*(10.0/ang_res_amin)**2*((1+z)/10.0)**(4.6)*(1.0/SpecRes*100.0/Tint)**(0.5)

	return noise_std_mK

def main():
	"Main() -> Running basic coeval mask with default settings. For full functionality, use as a module"
	get_obs_coeval()

if __name__ == '__main__':
	main()
