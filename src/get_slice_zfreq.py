#!/usr/bin/env python3
#
# Copyright (c) 2017 Weitian LI <weitian@aaronly.me> (adapted by Catherine Watkinson for use within PyObs21)
# MIT License
#

"""
Get the slices at the specified redshifts/frequencies from the HI
light-cone cube (created by `make_lightcone.py`), and use linear
interpolation.
"""

DEBUG=0 # controls how little or much the code prints out

import sys
#import argparse
#import logging
#from datetime import datetime, timezone

import numpy as np
from astropy.io import fits
from astropy.cosmology import FlatLambdaCDM

from pyobs21 import convs_dists as cvdist

freq21cm = 1420405751.7667 / 1e6  # [MHz]

#cw from z2freq import z2freq, freq2z


#logging.basicConfig(level=logging.INFO,
#                    format="[%(levelname)s:%(lineno)d] %(message)s")
#logger = logging.getLogger()

# cw copied from z2freq.py
def z2freq(redshifts, print_=False):
    redshifts = np.asarray(redshifts)
    freqs = cvdist.nu_from_z( redshifts )
    if print_:
        print("# redshift  frequency[MHz]")
        for z, f in zip(redshifts, freqs):
            print("%.4f  %.2f" % (z, f))
    return freqs

# cw copied from freq2z.py
def freq2z(freqs, print_=False):
    freqs = np.asarray(freqs)
    redshifts = cvdist.z_from_nu( freqs )
    if print_:
        print("# redshift  frequency[MHz]")
        for f, z in zip(freqs, redshifts):
           print("%.2f  %.4f" % (f, z))
    return redshifts

class LightCone:
    """
    Light-cone cube mimic the observation of HI signal.
    """
    def __init__(self, data_in, header_dict, cosmo=None):
        '''
        header_dict must be a dictionary containing the following entries:
            ["Nside"] number pixels on side (for the xy slices)
            ["Dc_step"] step size in comoving Mpc
            ["Dc_min"] comoving distance of highest freq/lowest z slice
            ["BUNIT"] unit of data mK or K

        data passed should be ordered according to x, y, nu
        '''
        #with fits.open(infile) as f:
        #self.data = np.swapaxes(data_in, 0, 2)
        self.data = data_in

        self.header = fits.Header()
        # we do the above because this is designed to work with fits file output (i.e. in z, y, x order)
        self.header.append( ("BUNIT", header_dict["BUNIT"],
                           "Data unit"), end=True )
        self.header.append( ("Lside", header_dict["Lside"],
                          "[cMpc] Simulation side length"), end=True)
        self.header.append( ("Nside", header_dict["Nside"],
                           "Number of cells at each side"), end=True)

        self.Dc_step=header_dict["Dc_step"]
        self.Dc_min=header_dict["Dc_min"]

        if (cosmo is None):
            self.cosmo = cvdist.cosmo_default()
        else:
            self.cosmo = cosmo
        if (DEBUG>0):
            print("Loaded light-cone cube: %dx%d (cells) * %d (slices)" % (self.Nside, self.Nside, self.Nslice))

    @property
    def Nslice(self):
        __, __,ns, = self.data.shape
        return ns

    @property
    def Nside(self):
        return self.header["Nside"]

    @property
    def slices_Dc(self):
        """
        The comoving distances of each slice in the light-cone cube.
        The slices are evenly distributed along the LoS with equal
        comoving step. [Mpc]
        """
        Dc_step = self.Dc_step
        Dc_min = self.Dc_min
        Dc = np.array( [Dc_min + Dc_step*i for i in range(self.Nslice)] )
        return Dc

    def get_slice(self, z):
        Dc = self.cosmo.comoving_distance(z + 1e-4).value  # [Mpc] the + 0.0001 is a slight hack to deal with numerical rounding making legitimate z requests fall outside Dc bounds
        if (DEBUG>0):
            print("In get_slice, z = ", z)
        slices_Dc = self.slices_Dc
        if Dc < slices_Dc.min() or Dc > slices_Dc.max():
            raise ValueError( "requested redshift (z=%e) out of range: Dc=%e which is outside the range %e < z < %e" % (z, Dc, slices_Dc.min(), slices_Dc.max()) )

        i2 = (slices_Dc <= Dc).sum()
        i1 = i2 - 1
        Dc1, s1 = slices_Dc[i1], self.data[:, :, i1]
        Dc2, s2 = slices_Dc[i2], self.data[:, :, i2]
        slope = (s2 - s1) / (Dc2 - Dc1)

        return s1 + slope * (Dc - Dc1)

    def write_slice(self, outfile, data, z, clobber=False, hdr_deg=None):
        '''
        hdr_deg (dict) - if you are outputting slices in degrees rather than the
                        class default (cMpc) you must contain:
            ["FoV_deg"] - FoV in degrees
            ["Nside"] - number pixels on a side
            header_dict['CRVAL1'] = FOV_OUT_deg # box size in degrees
            header_dict['CDELT1'] = ( FOV_OUT_deg/(NSIDE_deg+0.0) ) # pixel size in degrees
            header_dict['CRVAL2'] = FOV_OUT_deg # box size in degrees
            header_dict['CDELT2'] = ( FOV_OUT_deg/(NSIDE_deg+0.0) ) # pixel size in degrees
            header_dict['CRVAL3'] = nu_slice*1.0e6 # converting to Hz for GPR
        '''
        freq = z2freq(z)
        Dc = self.cosmo.comoving_distance(z).value  # [Mpc]
        head = fits.Header()

        # Add the header entries needed for working with GPR
        if (hdr_deg is None):
            head["Lside"] = (self.header["Lside"],
                               self.header.comments["Lside"])
            head["Nside"] = (self.header["Nside"],
                               self.header.comments["Nside"])
            head['CRVAL1'] = self.header["Lside"]
            head['CDELT1'] = self.header["Lside"]/( self.header["Nside"] + 0.0 )
            head['CRVAL2'] = head['CRVAL1']
            head['CDELT2'] = head['CDELT1']
            head['CRVAL3'] = freq*1.0e6
        else:
            head["Lside"] = (hdr_deg["FoV_deg"],
                               "[deg] Simulation side length")
            head["Nside"] = (hdr_deg["Nside"],
                               "Number of cells at each side")
            head['CRVAL1'] = hdr_deg["FoV_deg"]
            head['CDELT1'] = hdr_deg["FoV_deg"]/( hdr_deg["Nside"] + 0.0 )
            head['CRVAL2'] = head['CRVAL1']
            head['CDELT2'] = head['CDELT1']
            head['CRVAL3'] = freq*1.0e6

        # Also keep the original entries from astro tookbox
        head["BUNIT"] = (self.header["BUNIT"],
                           self.header.comments["BUNIT"])

        head["REDSHIFT"] = (z, "redshift of this slice")
        head["FREQ"] = (freq, "[MHz] observed HI signal frequency")
        head["Dc"] = (Dc, "[cMpc] comoving distance")
        #head["DATE"] = (datetime.now(timezone.utc).astimezone().isoformat(),
        #                  "File creation date")
        head.add_history(" ".join(sys.argv))
        data_out=data
        hdu = fits.PrimaryHDU(data=data_out, header=head)
        try:
            hdu.writeto(outfile, overwrite=clobber)
        except TypeError:
            hdu.writeto(outfile, clobber=clobber)
        if (DEBUG>0):
            print("Wrote slice to file: %s" % outfile)



def main():
    outfile_pattern = "{prefix}_f{freq:06.2f}_z{z:06.3f}.fits"
    outfile_prefix = "deltaTb"

    parser = argparse.ArgumentParser(
        description="Get slices at requested redshifts/frequencies " +
        "from light-cone cube")
    parser.add_argument("-C", "--clobber", dest="clobber",
                        action="store_true",
                        help="overwrite existing files")
    parser.add_argument("-i", "--infile", dest="infile", required=True,
                        help="input light-cone cube")
    parser.add_argument("-o", "--outfile", dest="outfile",
                        default=outfile_pattern,
                        help="output image slice filename pattern FITS " +
                        "(default: %s)" % outfile_pattern)
    parser.add_argument("-p", "--prefix", dest="prefix",
                        default=outfile_prefix,
                        help="prefix of output slices (default: %s)" %
                        outfile_prefix)
    exgrp = parser.add_mutually_exclusive_group(required=True)
    exgrp.add_argument("-z", "--redshifts", dest="redshifts", nargs="+",
                       help="redshifts where to interpolate slices")
    exgrp.add_argument("-f", "--freqs", dest="freqs", nargs="+",
                       help="21cm frequencies [MHz] to interpolate slices")
    args = parser.parse_args()

    if args.redshifts:
        redshifts = [float(z) for z in args.redshifts]
        freqs = z2freq(redshifts, print_=False)
    else:
        freqs = [float(f) for f in args.freqs]
        redshifts = freq2z(freqs, print_=False)

    lightcone = LightCone(args.infile)
    for z, f in zip(redshifts, freqs):
        outfile = args.outfile.format(prefix=args.prefix, z=z, freq=f)
        if (DEBUG>0):
            print("z=%06.3f, freq=%06.2f MHz : %s ..." % (z, f, outfile))
        data = lightcone.get_slice(z)
        lightcone.write_slice(outfile, data=data, z=z, clobber=args.clobber)


if __name__ == "__main__":
    main()
