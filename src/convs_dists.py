from astropy.cosmology import FlatLambdaCDM, z_at_value
import astropy.units as au
from astropy.io import fits
import numpy as np
from scipy import signal
from numpy.fft import fftn, ifftn, fftfreq, rfftfreq

'''
Contains common conversions, signal processing and cosmological distance calculations
common to the main modules

!!!! make kperp_from_uv() depend on cosmo like all others
'''

DEBUG=0

# CONSTANTS DO NOT ALTER!
TOCM_NU0 = 1420.40575177 # rest frame 21cm frequency (MHz)
TOCM_LAM0 = 2.998e8/(TOCM_NU0*1.0e6) #rest frame 21cm wavelength (m)
SPEEDOFLIGHT_CGS=2.99792458e10 # Speed of light in cgs
H0_CMSMPC=1.0e7 # H0 in units of cm/s/Mpc

def cosmo_default():
	return FlatLambdaCDM(H0=71, Om0=0.27)

def save_slice_4_GPR(data_slice=None, freq_MHz=None, FoV_deg=None, del_freq_MHz=None, Nside_deg=None, outfile_prefix=None, clobber=False):

	'''
	Saves a data slice as FITS with header info needed for use with GPR's load_from_fits
	GPR uses the header info as:
		freq = header['CRVAL3'] - frequency of slice in Hz
		res = abs(np.radians(header['CDELT1'])) - pixel size in degrees

	INPUTS:
	data_slice (n x n float) - 2D image you wish to save to FITS
	freq (float) - frequency of slice in MHz
	FoV_deg (float array) - size of each image side in degrees, if 1D then assumes cubic
	Nside_deg (float array) - number pixels on each side of image, if 1D then assumes cubic
	outfile_prefix (string opt) - base directory + any desired prefix in which to save FITS, if None defaults to working directory
	clobber (bool) (opt) - whether to overwrite an existing file with the same name

	OUTPUT:
	filepath
	'''

	if not(len(FoV_deg)==len(Nside_deg)):
		ValueError("FoV_deg and Nside_deg should be array of same dimension, 1D for cubic data and 2D for non-cubic data")
	head = fits.Header()
	if (len(FoV_deg)==1):
		head['CRVAL1'] = (FoV_deg[0], "[deg] Simulation side length")
		head['CDELT1'] = ( (FoV_deg[0]/( Nside_deg[0] + 0.0 )), "Number of cells at each side")
		head['CRVAL2'] = head['CRVAL1']
		head['CDELT2'] = head['CDELT1']
		head['CRVAL3'] = freq_MHz*1.0e6
		head['CDELT3'] = del_freq_MHz*1.0e6
		#head['NAXIS3'] = 1
	else:
		head['CRVAL1'] = (FoV_deg[0], "[deg] Simulation side length")
		head['CDELT1'] = ( (FoV_deg[0]/( Nside_deg[0] + 0.0 )), "Number of cells at each side")
		head['CRVAL2'] = (FoV_deg[1], "[deg] Simulation side length")
		head['CDELT2'] = ( (FoV_deg[1]/( Nside_deg[1] + 0.0 )), "Number of cells at each side")
		head['CRVAL3'] = freq_MHz*1.0e6
		head['CDELT3'] = del_freq_MHz*1.0e6
		#head['NAXIS3'] = 1

	hdu = fits.PrimaryHDU(data=data_slice, header=head)

	if (len(FoV_deg)==1):
		outfile_pattern = "{prefix}_gpr_slice_f{freq:06.2f}_{dim:d}_{fov:06.2f}deg.fits"
		if (outfile_prefix is None):
			outfile = outfile_pattern.format( prefix='', freq=freq_MHz, dim=Nside_deg[0], fov=FoV_deg[0] )
		else:
			outfile = outfile_pattern.format( prefix=outfile_prefix, freq=freq_MHz, dim=Nside_deg[0], fov=FoV_deg[0] )
	else:
		outfile_pattern = "{prefix}_gpr_slice_f{freq:06.2f}_{dimx:d}_{fovx:06.2f}deg_{dimy:d}_{fovy:06.2f}deg.fits"
		if (outfile_prefix is None):
			outfile = outfile_pattern.format( prefix='', freq=freq_MHz, dimx=Nside_deg[0], fovx=FoV_deg[0], dimy=Nside_deg[1], fovy=FoV_deg[1] )
		else:
			outfile = outfile_pattern.format( prefix=outfile_prefix, freq=freq_MHz, dimx=Nside_deg[0], fovx=FoV_deg[0], dimy=Nside_deg[1], fovy=FoV_deg[1] )


	try:
		hdu.writeto(outfile, overwrite=clobber)
	except TypeError:
		hdu.writeto(outfile, clobber=clobber)
	if (DEBUG>0):
		print("convs_dists.py (save_slice_4_GPR) - Wrote slice to file: %s" % outfile)

	return outfile

def nuttall_1D_window(nchan):
	name='nuttall'
	window_func = getattr(signal.windows, name)
	nfreq = nchan
	window = window_func(nfreq, sym=False)
	width_pix = nchan
	print("Generated window: %s (%d pixels)" % (name, width_pix))
	return window

def nd_windowfunc_nuttall(data):
	"""
	Performs an in-place Nutall windowing on N-dimensional spatial-domain data.
	This is done to mitigate boundary effects in the FFT.

	Parameters
	----------
	[data] - ndarray
		   Input data to be windowed, modified in place.
	"""

	for axis, axis_size in enumerate(data.shape):
		# set up shape for numpy broadcasting
		filter_shape = [1, ] * data.ndim
		filter_shape[axis] = axis_size
		#window = np.kaiser(axis_size, beta).reshape(filter_shape)
		window = signal.nuttall(axis_size, sym=False).reshape(filter_shape)
		# scale the window intensities to maintain image intensity with repeat application of same WF
		window=window**(1.0/data.ndim)
		data *= window

def nd_windowfunc_tukey(data, beta):
	"""
	Performs an in-place tukey windowing on N-dimensional spatial-domain data.
	This is done to mitigate boundary effects in the FFT.

	Parameters
	----------
	[data] - ndarray
		   Input data to be windowed, modified in place.
	[beta] : shape parameter for window 0 is rectangle
	"""
	for axis, axis_size in enumerate(data.shape):
		# set up shape for numpy broadcasting
		filter_shape = [1, ] * data.ndim
		filter_shape[axis] = axis_size
		#window = np.kaiser(axis_size, beta).reshape(filter_shape)
		window = signal.tukey(axis_size, beta, sym=False).reshape(filter_shape)
		# scale the window intensities to maintain image intensity with repeat application of same WF
		window=window**(1.0/data.ndim)
		data *= window

def nd_windowfunc_kaiser(data, beta):
	"""
	Performs an in-place tukey windowing on N-dimensional spatial-domain data.
	This is done to mitigate boundary effects in the FFT.

	Parameters
	----------
	[data] - ndarray
		   Input data to be windowed, modified in place.
	[beta] : shape parameter for window 0 is rectangle
	"""
	for axis, axis_size in enumerate(data.shape):
		# set up shape for numpy broadcasting
		filter_shape = [1, ] * data.ndim
		filter_shape[axis] = axis_size
		window = np.kaiser(axis_size, beta).reshape(filter_shape)
		#window = signal.tukey(axis_size, beta, sym=False).reshape(filter_shape)
		# scale the window intensities to maintain image intensity with repeat application of same WF
		window=window**(1.0/data.ndim)
		data *= window

def z_from_nu(nu):
	# Returns z from nu (MHz)
	return ( TOCM_NU0/nu - 1.0 )

def nu_from_z(z):
	# returns observed frequency (MHz) for 21cm line at a given redshift z
	return ( TOCM_NU0/(1.0+z) )

def z_from_lambda(lam_obs):
	# Returns z given the observed wavelength of 21cm line, lambda (m)
	return lam_obs/TOCM_LAM0 - 1.0

def lamda_from_z(z):
	# returns observed wavelength (m) for 21cm line at a given redshift z
	return TOCM_LAM0*(1.0+z)

def cMpc_from_dz(zmin=None, zmax=None, cosmo=None):
	if cosmo is None:
		cosmo = cosmo_default()

	return (cosmo.comoving_distance(zmax).value - cosmo.comoving_distance(zmin).value )

def delnu_from_cMpc(cMpc, z_mid, cosmo=None):
	'''
	Inverts eqn (4) in Furlanetto et al. 2006 review for delnu (MHz) and delr (cMpc)
	'''
	if cosmo is None:
		cosmo = cosmo_default()

	dist_to_zmid = cosmo.comoving_distance(z_mid).value
	cdist_low = dist_to_zmid - cMpc/2.0
	cdist_hi = dist_to_zmid + cMpc/2.0

	z_low = z_at_value( cosmo.comoving_distance, cdist_low*au.Mpc )
	z_hi = z_at_value( cosmo.comoving_distance, cdist_hi*au.Mpc )

	del_nu = nu_from_z( z_low ) - nu_from_z( z_hi )

	return ( del_nu )

def cMpc_from_theta_degrees(theta_deg, z, cosmo=None):
	'''
	Reference: [cMpc]/arcmin conversion from Furlanetto et al. (2006)'''

	if cosmo is None:
		cosmo = cosmo_default()

	hlittle = cosmo.H0.value/100.0
	return 1.9 * ( theta_deg*60.0 ) * ( (1+z) / 10. )**.2 / hlittle #here we have converted degrees to arcmins

def Dm_cc(z=None, cosmo=None, omega_m=None, hlittle=None):
	'''
		Returns the astropy estimation of comoving distance to z
	'''

	if (z is None):
		print("conv_dists.Dm_cc -> z must be passed. Usage: Dm_cc(z, *omega_m, *hlittle)")

	if (cosmo is None):
		if not(omega_m is None):
			if (DEBUG>1):
				print("overriding cosmo default Om0")
			Omm = omega_m
			if not( hlittle is None):
				if (DEBUG>1):
					print("overriding cosmo default H0")
				H0 = hlittle*100.0
			else:
				H0 = 71
		else:
			Omm = 0.27
		cosmo=FlatLambdaCDM(H0=71, Om0=0.27)

	#cosmocalc_dict = cosmocalc(z, 100.0*hlittle, omega_m, 1.0-omega_m)
	return cosmo.comoving_distance( z ).value #cosmocalc_dict['DCMR_Mpc']

def klos_from_eta(eta_cMHz, z_mid, cosmo=None, omega_m=None, hlittle=None):
	'''
	Returns k_los = 2pi/(\delta x) in cMpc^{-1} given \eta = 2pi/(\delta nu) in MHz

	Given the reciprocal relation to scale x and frequency nu,
	this is identical to delnu_from_cMpc(eta_cMHz) multiplied by 2pi
	(Ref: Vedantham, Shankar, Subrahmanyan 2012 https://iopscience.iop.org/article/10.1088/0004-637X/745/2/176/pdf)
	'''

	if (cosmo is None):
		if not(omega_m is None):
			if (DEBUG>1):
				print("overriding cosmo default Om0")
			Omm = omega_m
			if not( hlittle is None):
				if (DEBUG>1):
					print("overriding cosmo default H0")
				H0 = hlittle*100.0
			else:
				H0 = 71
		else:
			Omm = 0.27

		cosmo=FlatLambdaCDM(H0=71, Om0=0.27)

	return ( 2.0*np.pi*delnu_from_cMpc(cMpc=eta_cMHz, z_mid=z_mid, cosmo=cosmo) ) #cw checked 1st oct

def kperp_from_uv(u_or_v, z, cosmo=None, omega_m=0.3075, hlittle=0.6774, Dm=None):
	'''
	This returns the comoving k on the sky given a u or v component.
	Dm is the comoving distance, this can be precalculated and passed to increase speed on multiple calls
	'''
	if (cosmo is None):
		if not(omega_m is None):
			if (DEBUG>1):
				print("overriding cosmo default Om0")
			Omm = omega_m
			if not( hlittle is None):
				if (DEBUG>1):
					print("overriding cosmo default H0")
				H0 = hlittle*100.0
			else:
				H0 = 71
		else:
			Omm = 0.27

		cosmo=FlatLambdaCDM(H0=71, Om0=0.27)

	if (Dm is None):
		Dm = Dm_cc( z=z, cosmo=cosmo )

	return (2*np.pi/Dm)*u_or_v
