from pyobs21 import convert_lightcones as conv_lc
from pyobs21 import convs_dists as conv

from astropy.cosmology import FlatLambdaCDM
import numpy as np
import pickle

REGEN_TEST_DATA = False # If you have done something to change the code knowingly, i.e. maybe improved accuracy, then you need to refresh test data

cosmo = FlatLambdaCDM(H0=71, Om0=0.27)

def lc_deg_2_cMpc():
    FoV_deg = 10.0
    Nside_deg = 1024
    BOX_LEN = 256.0
    HII_DIM = 128

    # load coevals hard coded data
    in_str="tests/hardcoded_results/coeval_z6.32167913284.pkl"
    f = open( in_str, 'rb' )
    hc_coev_6 = pickle.load( f )
    f.close()

    in_str="tests/hardcoded_results/coeval_z9.00285740683.pkl"
    f = open( in_str, 'rb' )
    hc_coev_9 = pickle.load( f )
    f.close()

    # Run lc_deg_2_cMpc on coevals
    # Make a header dict for lightcone class in PyObs21
    hdr_dict = conv_lc.make_header( data=hc_coev_6, z_ctr=6.32167913284, L_perp=BOX_LEN, L_los=BOX_LEN )
    lc_6, redshifts, freqs, files = conv_lc.lc_cMpc_2_deg(data_in=hc_coev_6, header_dict=hdr_dict, FoV_deg=FoV_deg, Nside_deg=Nside_deg, outfile_prefix=False)

    hdr_dict = conv_lc.make_header( data=hc_coev_6, z_ctr=9.00285740683, L_perp=BOX_LEN, L_los=BOX_LEN )
    lc_9, redshifts, freqs, files = conv_lc.lc_cMpc_2_deg(data_in=hc_coev_9, header_dict=hdr_dict, FoV_deg=FoV_deg, Nside_deg=Nside_deg, outfile_prefix=False)

    if (REGEN_TEST_DATA is True): # Refresh the hard-coded data
        out_str = "tests/hardcoded_results/lc_deg_z6.32167913284.pkl"
        f = open(out_str , 'wb' )
        pickle.dump( lc_6, f )
        f.close()
        out_str = "tests/hardcoded_results/lc_deg_z9.00285740683.pkl"
        f = open(out_str , 'wb' )
        pickle.dump( lc_6, f )
        f.close()

    # load lc_obs hard coded data
    in_str="tests/hardcoded_results/lc_deg_z6.32167913284.pkl"
    f = open( in_str, 'rb' )
    hc_lc_deg_6 = pickle.load( f )
    f.close()

    in_str="tests/hardcoded_results/lc_deg_z9.00285740683.pkl"
    f = open( in_str, 'rb' )
    hc_lc_deg_9 = pickle.load( f )
    f.close()

    new_data_flat = lc_6.flatten()
    test_data_flat = hc_lc_deg_6.flatten()

    # loop over all pixels of output and assert they are roughly the same as lc_obs
    for (j, test_j) in enumerate( test_data_flat ):
        if (np.abs(test_j>0.0 and new_data_flat[j]>0.0) ):
            assert ( np.abs(test_j - new_data_flat[j]) < 1e-4 )
        else:
            assert ( np.abs(test_j/new_data_flat[j] - 1) < 1e-4 )

    new_data_flat = lc_9.flatten()
    test_data_flat = hc_lc_deg_9.flatten()

    # loop over all pixels of output and assert they are roughly the same as lc_obs
    for (j, test_j) in enumerate( test_data_flat ):
        if (np.abs(test_j>0.0 and new_data_flat[j]>0.0) ):
            assert ( np.abs(test_j - new_data_flat[j]) < 1e-4 )
        else:
            assert ( np.abs(test_j/new_data_flat[j] - 1) < 1e-4 )


def lc_deg_2_cMpc():

    FoV_deg = 10.0
    Nside_deg = 1024
    BOX_LEN = 256.0
    HII_DIM = 128

    # load lc_cMpc hard-coded data
    in_str="tests/hardcoded_results/lc_deg_z6.32167913284.pkl"
    f = open( in_str, 'rb' )
    hc_lc_deg_6 = pickle.load( f )
    f.close()

    in_str="tests/hardcoded_results/lc_deg_z9.00285740683.pkl"
    f = open( in_str, 'rb' )
    hc_lc_deg_9 = pickle.load( f )
    f.close()

    in_str="tests/hardcoded_results/redshifts_list_zctrl6.32167913284.pkl"
    f = open( in_str, 'rb' )
    zs_6 = pickle.load( f )
    f.close()

    in_str="tests/hardcoded_results/redshifts_list_zctrl9.00285740683.pkl"
    f = open( in_str, 'rb' )
    zs_9 = pickle.load( f )
    f.close()

    lc_cMpc_6 = conv_lc.lc_deg_2_cMpc(data_in=hc_lc_deg_6, FoV_deg=FoV_deg, L_cMpc=BOX_LEN, Nside_cMpc=HII_DIM, redshifts=zs_6, cut_ctrl=True)
    lc_cMpc_9 = conv_lc.lc_deg_2_cMpc(data_in=hc_lc_deg_9, FoV_deg=FoV_deg, L_cMpc=BOX_LEN, Nside_cMpc=HII_DIM, redshifts=zs_9, cut_ctrl=True)

    if (REGEN_TEST_DATA is True): # Refresh the hard-coded data
        out_str = "tests/hardcoded_results/lc_cMpc_z6.32167913284.pkl"
        f = open(out_str , 'wb' )
        pickle.dump( lc_cMpc_6, f )
        f.close()
        out_str = "tests/hardcoded_results/lc_cMpc_z9.00285740683.pkl"
        f = open(out_str , 'wb' )
        pickle.dump( lc_cMpc_9, f )
        f.close()

    # load lc_obs hard-coded data
    in_str="tests/hardcoded_results/lc_cMpc_z6.32167913284.pkl"
    f = open( in_str, 'rb' )
    hc_lc_cMpc_6 = pickle.load( f )
    f.close()

    in_str="tests/hardcoded_results/lc_cMpc_z9.00285740683.pkl"
    f = open( in_str, 'rb' )
    hc_lc_cMpc_9 = pickle.load( f )
    f.close()

    # Run lc_deg_2_cMpc on lc_obs

    # loop over all pixels of output and assert they are roughly the same as lc_cMpc
    new_data_flat = lc_cMpc_6.flatten()
    test_data_flat = hc_lc_cMpc_6.flatten()
    for (j, test_j) in enumerate( test_data_flat ):
        if (np.abs(test_j>0.0 and new_data_flat[j]>0.0) ):
            assert ( np.abs(test_j - new_data_flat[j]) < 1e-4 )
        else:
            assert ( np.abs(test_j/new_data_flat[j] - 1) < 1e-4 )

    new_data_flat = lc_cMpc_9.flatten()
    test_data_flat = hc_lc_cMpc_9.flatten()
    for (j, test_j) in enumerate( test_data_flat ):
        if (np.abs(test_j>0.0 and new_data_flat[j]>0.0) ):
            assert ( np.abs(test_j - new_data_flat[j]) < 1e-4 )
        else:
            assert ( np.abs(test_j/new_data_flat[j] - 1) < 1e-4 )


def test_delnu_from_cMpc():
    assert ( np.abs(conv.delnu_from_cMpc(cMpc=200.0, z_mid=15, cosmo=cosmo) - 8.742708163593875) <1e-12 )
    assert ( np.abs(conv.delnu_from_cMpc(cMpc=100.0, z_mid=15, cosmo=cosmo) - 4.371348890969728) <1e-12 )
    assert ( np.abs(conv.delnu_from_cMpc(cMpc=200.0, z_mid=8, cosmo=cosmo) - 11.674727340539533) <1e-12 )
    assert ( np.abs(conv.delnu_from_cMpc(cMpc=100.0, z_mid=8, cosmo=cosmo) - 5.837342654810442) <1e-12 )

def test_cMpc_coeval_2_deg():

    FOV_OUT = 39.0 # desired FoV of output slice in Degrees (if you don't want to tile, make sure this corresponds to less than smallest FoV in your dataset)
    NSIDE = 550 # Desired number of pixels on a side

    Lx= (1000.0/0.6774)
    indx=[0, 120, 224]
    z_slice=[0.5736494673033408, 0.8377191443911786, 1.1036703293709729]
    for ct, i in enumerate( indx ):

        in_str = "tests/hardcoded_results/conv_test_in_i"+str(i)+"_225_1cGpc.pkl"
        f = open( in_str, 'rb' )
        in_slice = pickle.load( f, encoding='bytes' )
        # py27 in_slice = pickle.load( f )
        f.close()

        out_slice, dim_trim = conv_lc.slice_cMpc_2_deg( data_slice=in_slice, Lin_cMpc=Lx, z=z_slice[ct], Lout_deg=FOV_OUT, dim_out=NSIDE, cut_ctrl=True, cosmo=FlatLambdaCDM(H0=70, Om0=0.27) )

        test_str = "tests/hardcoded_results/conv_test_out_i"+str(i)+"_225_1cGpc.pkl"
        if (REGEN_TEST_DATA is True):
            f = open( test_str, 'wb' )
            pickle.dump( out_slice, f )
            f.close()

        f = open( test_str, 'rb' )
        test_slice = pickle.load( f, encoding='bytes' )
        # py27 test_slice = pickle.load( f )
        f.close()

        out_slice = out_slice.flatten()
        test_slice = test_slice.flatten()

        for (j, test_d) in enumerate( test_slice ):
            assert (out_slice[j] == test_d)

def test_deg_2_cMpc():
    FOV_OUT = 39.0 # desired FoV of output slice in Degrees (if you don't want to tile, make sure this corresponds to less than smallest FoV in your dataset)
    Lx= (1000.0/0.6774)
    FOV_OUT_cMpc = Lx*0.9
    NSIDE = 550 # Desired number of pixels on a side

    indx=[0, 120, 224]
    z_slice=[0.5736494673033408, 0.8377191443911786, 1.1036703293709729]
    for ct, i in enumerate( indx ):
        in_str = "tests/hardcoded_results/conv_test_in_i"+str(i)+"_225_1cGpc.pkl"
        f = open( in_str, 'rb' )
        in_slice = pickle.load( f, encoding='bytes' )
        # py27 in_slice = pickle.load( f )
        f.close()

        out_slice, dim_trim = conv_lc.slice_cMpc_2_deg( data_slice=in_slice, Lin_cMpc=Lx, z=z_slice[ct], Lout_deg=FOV_OUT, dim_out=NSIDE, cut_ctrl=True, cosmo=FlatLambdaCDM(H0=70, Om0=0.27) )
        cMpc_slice, dim_trim = conv_lc.slice_deg_2_cMpc( data_slice=out_slice, Lin_deg=FOV_OUT, z=z_slice[ct], Lout_cMpc=FOV_OUT_cMpc, dim_out=NSIDE, cut_ctrl=True, cosmo=FlatLambdaCDM(H0=70, Om0=0.27) )

        test_str = "tests/hardcoded_results/conv_test_out_deg2cMpc_i"+str(i)+"_225_1cGpc.pkl"
        if (REGEN_TEST_DATA is True):
            f = open( test_str, 'wb' )
            pickle.dump( cMpc_slice, f )
            f.close()
        f = open( test_str, 'rb' )
        test_slice = pickle.load( f, encoding='bytes' )
        # py27 test_slice = pickle.load( f )
        f.close()

        assert ( np.mean(cMpc_slice)==np.mean(test_slice) )
        assert ( np.var(cMpc_slice)==np.var(test_slice) )

        cMpc_slice = cMpc_slice.flatten()
        test_slice = test_slice.flatten()

        for (j, test_d) in enumerate( test_slice ):
            assert (cMpc_slice[j] == test_d)
