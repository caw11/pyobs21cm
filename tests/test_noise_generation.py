import os, sys
lib_path = os.path.abspath('../')
sys.path.append(lib_path)

import py21cmfast as p21
import pyobs21
import numpy as np
import pickle
from powerbox.tools import get_power

REGEN_TEST_DATA = False

NITER = 10
BOX_DIM = 128
BOX_LEN = 256

def test_random_sample():
    var_scaling = 10000
    Pnoise = np.ones( [BOX_DIM, BOX_DIM, BOX_DIM], dtype=complex )*var_scaling
    stacked_kbox = np.zeros( [BOX_DIM, BOX_DIM, BOX_DIM], dtype=complex )
    for i in range(NITER):
        noise_kspace = pyobs21.random_sample_noise( Pnoise )
        if (i==0):
            var_real = np.var( np.real( noise_kspace ) )
            var_imag = np.var( np.imag( noise_kspace ) )

        assert (   np.mean( np.real( noise_kspace ) ) < 2e-5*var_scaling  )
        assert (  np.abs( np.var( np.real( noise_kspace ) ) - var_scaling  )/var_scaling < 0.01 )
        assert (   np.mean( np.imag( noise_kspace ) ) < 2e-5*var_scaling  )
        assert (  np.abs( np.var( np.imag( noise_kspace ) )  - var_scaling )/var_scaling < 0.01 )

        '''for ind, value in np.ndenumerate(noise_kspace):
            stacked_kbox[ind] += value'''
        stacked_kbox = stacked_kbox + noise_kspace
        

        if ( (np.abs(var_real/(i+1.0)-np.var( np.real(stacked_kbox)/(i+1.0) ) ))/ var_real/(i+1.0) > 0.01 ):
            print("danger, danger!!! fractional drift from theoretical noise (real) avging down = ", np.abs(var_real-np.var( np.real(stacked_kbox/(i+1.0)) ))/var_real/(i+1.0) )

        assert ( (np.abs(var_real/(i+1.0)-np.var( np.real(stacked_kbox)/(i+1.0) ) ))/ var_real/(i+1.0) <= 0.01 )

        if ( (np.abs(var_imag/(i+1.0)-np.var( np.imag(stacked_kbox)/(i+1.0) ) ))/ var_imag/(i+1.0) > 0.01 ):
            print("danger, danger!!! fractional drift from theoretical noise (imag) avging down = ", np.abs(var_imag-np.var( np.imag(stacked_kbox/(i+1.0)) ))/var_imag/(i+1.0) )
        assert ( (np.abs(var_imag/(i+1.0)-np.var( np.imag(stacked_kbox)/(i+1.0) ) ))/ var_imag/(i+1.0) <= 0.01 )

def test_noise_power_spec():

    INST_BASE = 'tests/hardcoded_results/'
    Inst_file=[INST_BASE+'ska_central_2019.track_1.0hr_opt_z6.322_nu0.194_nchan128_bwidth0.0169_256cMpc.h5',
               INST_BASE+'ska_central_2019.track_1.0hr_opt_z7.025_nu0.177_nchan128_bwidth0.0162_256cMpc.h5',
               INST_BASE+'ska_central_2019.track_1.0hr_opt_z8.047_nu0.157_nchan128_bwidth0.0152_256cMpc.h5',
               INST_BASE+'ska_central_2019.track_1.0hr_opt_z9.003_nu0.142_nchan128_bwidth0.0145_256cMpc.h5']
    HC_FILEBASE = INST_BASE+'noise_power_spec'
    RED = [6.322, 7.025, 8.047, 9.003]

    for i, fl in enumerate(Inst_file):
        fl_hc = HC_FILEBASE+'z'+str(RED[i])

        data_coeval = pyobs21.get_obs_coeval( Filepath=fl, z=RED[i], Dim=BOX_DIM, Len=BOX_LEN, sim_noise=True, Psv=None, sim_coeval=None, noise_seed=12 )

        res = get_power(data_coeval, boxlength=BOX_LEN)

        res = list(res)
        k = res[1]

        mask = np.logical_and(k <= 1.5, k >= 0.1)
        if (REGEN_TEST_DATA is True):
            f = open( fl_hc, 'wb' )
            pickle.dump( [k[mask], res[0][mask]*k[mask]**3/(2*np.pi**2)], f )
            f.close()

        f = open( fl_hc, 'rb' )
        k_Pk_hc = pickle.load( f )
        f.close()

        assert np.all( k_Pk_hc[0] == k[mask] )
        assert np.all( np.abs(k_Pk_hc[1] - res[0][mask]*k[mask]**3/(2*np.pi**2) ) < 1e-5 )
