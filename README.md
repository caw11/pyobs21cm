# PyObs21
## Package to simulate radio-interferometer observations from simulated brightness temperature coevals in cMpc

Written  by C. Watkinson (catherine.watkinson@gmail.com).
If you make use of this, please reference this repository.

This code is designed to be used alongside 21cmMC like codes that produce datacubes and 21cmSENSE to simulate observational boxes.

It can also be used to output GPR compatible slices.

PyObs21 currently applies uv sampling (which can exclude wedge corrupted modes if desired)
to co-moving 21cmMC assuming a fixed redshift when converting from u, v and nu to kx, ky and kz.
Future updates will apply primary beam smoothing and uv sampling to a 21cmMC
lightcone on a slice-by-slice basis along the freq axis (to include wedge effects).

--------------------------------------------------------------------------------------
## simObs_PyObs21
Offers higher-level functionality including the ability
to produce uv masks in comoving coords and generate noise realisations.
After the first call for a given instrument & freq, a uvsampling-noise mask is saved to file so
that subsequent calls are substantially faster.

--------------------------------------------------------------------------------------
## getstats_PyObs21
This contains a bunch of wrapper functions which calculate observed statistics.

It can also be used to generate 21cmMC-friendly noise files containing noise error on
the power spectrum and bispectrum, as well as 21cmMC-friendly datafiles.
This can be easily extended to calculate the noise error on other statistics.

--------------------------------------------------------------------------------------
## Auxillary codes
convs_dists.py - Contains functions for various conversions needed for the main code
cosmocalc_py3.py - python 3 friendly version of cosmocalc

--------------------------------------------------------------------------------------
## Example driver scripts
There is driver_pspec_example_two_param_coeval.py and driver_bispec_example_two_param_coeval.py
show how to use the code at it's most basic level.

driver_pspec_noise_file.py and driver_bispec_noise_file.py generate noise-error
and data files for use with 21cmMC.

All drivers have the CMSENSE_FLAGS_ONLY. If this is set to be True, then the driver
bypasses all of the main functions of the driver and instead returns
flags for use with Uv_sampling_and_noise/uv_to_k_plus_calc_sense.py (see this folder
and the below for info on using this module)
that are consistent with your 21cmMC simulation resolution.

--------------------------------------------------------------------------------------
## src/Uv_sampling_and_noise/uv_to_k_plus_calc_sense.py
This contains some precalculated uvsampling-noise masks so you can just jump in.
They are to be used with a box of 128^3 and (256cMpc)^3 and for
redshifts = [6.32167913284,7.02489125294,8.04717039344,9.00285740683].
To generate new uvsampling-noise files (the corresponding masks get generated
automatically when you first call simObs_PyObs21) for different simulation
properties and redshifts, see the README in Uv_sampling_and_noise/ for instructions.

--------------------------------------------------------------------------------------
## simObs_PyObs21.py useful functions


get_obs_coeval - produces a uv-sampled version of passed coeval sim with or without
                 instrumental noise added on top (note this ignores los evolution
                 with the exception the foreground wedge corruption).

get_obs_fft_coeval - as above but in k-space.
                     Can be used to quickly generate multiple noise realisations over a simulation.
                     This can also be used to generate a uv-sampling noise mask
                     in simulation co-ordinates (which can also be acquired in real space
                     by switching off noise sims and not passing a simulation to get_obs_coeval).
                     uv-sampling noise mask contains the noise error or number of samples per cell
                     depending on settings.


--------------------------------------------------------------------------------------
# PyObs21cm Dependencies
--------------------------------------------------------------------------------------

Python 3

21cmMC (designed for use with development branch - https://github.com/BradGreig/Hybrid21CM/tree/develop-steven)

Numpy

Scipy

astropy

## Optional for generating new noise sampling files other than those packaged:

Python 2.7

21cmSENSE (https://github.com/jpober/21cmSense)

Aipy

--------------------------------------------------------------------------------------
# PyObs21cm Installation notes
--------------------------------------------------------------------------------------
simObs_PyObs21.py and getstats_PyObs21.py should work out of the box from the main directory
so long as you call with default user params and redshifts. For others you need to use
Uv_sampling_and_noise/uv_to_k_plus_calc_sense.py to generate new uvsampling-noise files.

To use getstats_PyObs21.py you also need update the SharedLibraryLocation for the power spectrum
and bispectrum C code in getstats_PyObs21, before compiling the C-code used to measure the
power spec and bispec with:

cd to main directory, compile C code:
    make

--------------------------------------------------------------------------------------

# PyObs21cm development notes

Some DEVEL_DESC flags are include in the parts of the code that are relevant and
will need adjusting for future developments.

--------------------------------------------------------------------------------------

-----------------------------
# PyObs21cm v1 - Public Release
-----------------------------
Simple co-moving implementation that applies uv-sampling and noise to a datacube
in simulation co-ordinates, i.e. cMPc

To-do v1:
Primary beam smoothing (optional given small sim volumes relative to instrument FoV)

DEVEL_SAMPVAR
At the moment I am approximating the effects of sample variance by treating it as
a noise term, i.e. by adding it's contribution to the instrumental noise sigma which is used to
create random samplings of noise.
This approximation is terrible as there will be correlations in the sample-variance noise between scales.
i.e. the noise due to sample variance is not i.i.d.
Nonetheless, this remains a useful and cheap way of using MCMC to calculate noise+sv error.

---------------------------------
# PyObs21cm v2 - Future development
---------------------------------

DEVEL_LIGHTCONE
Add a simple co-moving lightcone implementation that applies uv sampling
on a slice-by-slice basis without the requirement that the data be cubic as for v1
and including evolution of uv sampling.

To do:
1. check format of 21cmMC lightcone mode
2. integrate uv_to_k_plus_calc_sense calculations inside main code so it maybe
applied on a slice-by-slice basis. This can be easily checked against the
original implementation. Although need to think if we can
