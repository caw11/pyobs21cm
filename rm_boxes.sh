#!/bin/bash

# This can be run to delete old boxes created by 21cmMC in error from older version
# causing a memory problem when using a different random seed on each call
# (this should be unnecessary for up to date installations)
# Also clears the maskfiles, useful when the code has been updated
# (or for any other reason) and you wish to recalculate noise or uv-sampling


rm Uv_sampling_and_noise/*_maskfile.h5

counter=1
while [ $counter -le 10 ]
do
  find ~/21CMMC_Boxes/ -type f -cmin +20 -print -exec rm {} +
  sleep 1200
done
